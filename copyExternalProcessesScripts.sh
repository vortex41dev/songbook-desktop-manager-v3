#!/bin/bash

cd dist;

requiredModules=(safe-buffer ultron ws bluetooth-serial-port);

copyNodeModules() {
    mkdir -p "$1/node_modules";
    for module in ${requiredModules[*]}
    do
        cp -r "./../node_modules/$module" "$1/node_modules/"
    done;
}

for i in *
do
    cp "../server.js" "$i/";
    cp "../btController.js" "$i/";
    copyNodeModules "$i"
done;
