#!/usr/bin/node

const finalHandler = require('finalhandler');
const http = require('http');
const serveStatic = require('serve-static');

console.log('Starting resource server...');

// Serve up resources folder
const serve = serveStatic('dist/resources', { 'index': ['index.html', 'index.htm'] });

const server = http.createServer((req, res) => {
  console.log("Got " + req.method + "request from " + req.headers.host + " for " + req.url);
  serve(req, res, finalHandler(req, res));
});

const serverPort = parseInt(process.argv[2]) || 10800;
console.log("Starting resource server to listen at " + serverPort);
server.listen(serverPort);
