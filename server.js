const IPC_MESSAGE_TYPES = {
    HELLO: 'HELLO',
    GOT_CLIENT: 'GOT_CLIENT',
    CLIENT_DISCONNECTED: 'CLIENT_DISCONNECTED',
    CLIENTS_LIST: 'CLIENTS_LIST'
};

const MESSAGE_CODES = {
    MSG_CHANGE_SLIDE: 1,
    MSG_CHANGE_SONG: 2,
    MSG_CHANGE_SLIDES_VISIBILITY: 3,

    MSG_CHANGE_SLIDE_CODE_PREV: 2,
    MSG_CHANGE_SLIDE_CODE_NEXT: 3,
    MSG_CHANGE_SLIDE_CODE_RESTART: 4,
};

class InterprocessMessage {
    constructor(messageType = '', data = {}) {
        this.messageType = messageType;
        this.data = data;
    }
}

const clients = [];

process.on('message', (msg) => {
    console.log("Got message " + JSON.stringify(msg));
    switch (msg.messageType) {
        case IPC_MESSAGE_TYPES.HELLO:
            process.send(new InterprocessMessage(IPC_MESSAGE_TYPES.HELLO));
            break;
        case IPC_MESSAGE_TYPES.CLIENTS_LIST:
            process.send(new InterprocessMessage(
                IPC_MESSAGE_TYPES.CLIENTS_LIST,
                clients.map(c => c._socket.remoteAddress)
            ));
            break;
    }
});

console.log("Starting server process... [" + process.pid + "]");

Object.defineProperty(exports, "__esModule", {value: true});
const WebSocket = require("ws");
const WebSocketServer = WebSocket.Server;
const messages = {
    hello: {
        messageType: 0,
        data: "hello"
    },
    update: {
        messageType: 1,
        data: 0
    },
};
const serverSock = new WebSocketServer({port: process.argv[2]});

const state = {
    song: null,
    visibility: false,
};

function updateState(dataJson) {
    switch (dataJson.messageType) {
        case MESSAGE_CODES.MSG_CHANGE_SONG:
            state.song = dataJson.data;
            if (!state.song.song.currentSectionNr) {
                state.song.song.currentSectionNr = 0;
            }
            break;
        case MESSAGE_CODES.MSG_CHANGE_SLIDE:
            switch (dataJson.data.dir) {
                case MESSAGE_CODES.MSG_CHANGE_SLIDE_CODE_NEXT:
                    if (state.song && state.song.song.currentSectionNr < state.song.song.sections.length) {
                        state.song.song.currentSectionNr++;
                    }
                    break;
                case MESSAGE_CODES.MSG_CHANGE_SLIDE_CODE_PREV:
                    if (state.song && state.song.song.currentSectionNr > 0) {
                        state.song.song.currentSectionNr--;
                    }
                    break;
                case MESSAGE_CODES.MSG_CHANGE_SLIDE_CODE_RESTART:
                    if (state.song) {
                        state.song.song.currentSectionNr = 0;
                    }
                    break;
            }
            break;
        case MESSAGE_CODES.MSG_CHANGE_SLIDES_VISIBILITY:
            state.visibility = dataJson.data;
            break;
    }
    console.log("State changed: ");
    console.log(state);
}

function sendState(state, ws) {
    ws.send(JSON.stringify({messageType: MESSAGE_CODES.MSG_CHANGE_SONG, data: state.song}));
    ws.send(JSON.stringify({messageType: MESSAGE_CODES.MSG_CHANGE_SLIDES_VISIBILITY, data: state.visibility}));
}

serverSock.on('connection', (ws) => {
    console.log("Got client " + ws._socket.remoteAddress);
    clients.push(ws);
    process.send(new InterprocessMessage(
        IPC_MESSAGE_TYPES.GOT_CLIENT,
        ws._socket.remoteAddress
    ));
    ws.remoteAddress = ws._socket.remoteAddress;
    ws.on('message', (message) => {
        console.log("Recvd msg from %s: %s", ws._socket.remoteAddress, message);
        try {
            const dataJson = JSON.parse(message);
            updateState(dataJson);
        } catch (e) {
            console.error(e);
        }
        sendUpdateMsgs(message, clients, ws);
    });
    ws.on('close', (code, reason) => {
        console.log("Client %s disconnected", ws.remoteAddress);
        process.send(new InterprocessMessage(
            IPC_MESSAGE_TYPES.CLIENT_DISCONNECTED,
            ws.remoteAddress
        ));
        clients.splice(clients.indexOf(ws), 1);

    });
    ws.send(JSON.stringify(messages.hello));

    if (state.song) {
        sendState(state, ws);
    }
});
function sendUpdateMsgs(message, clients, sender) {
    clients.forEach(ws => {
        if (sender !== ws) {
            console.log("Sending update ");
            ws.send(message);
        }
    });
}
