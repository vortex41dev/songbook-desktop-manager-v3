const destBtAddress = process.argv[2];
const destBtChannel = process.argv[3];

console.log(destBtAddress);
console.log(destBtChannel);


process.on('message', (msg) => {
  console.log("BT: Got message " + JSON.stringify(msg));
});

function sendIpcMsg(type, data) {
  let message = { messageType: type, data: data };
  if (process.send) {
    process.send(message);
  } else {
    console.log(message);
  }
}

function sendConnectionState(state, data) {
  sendIpcMsg('state', { state: state, data: data });
}

function sendCommand(command) {
  sendIpcMsg('command', command);
}

sendConnectionState('setup');

sendConnectionState('connecting');

try {
  const BluetoothSerial = require('bluetooth-serial-port');
  const btSerial = new BluetoothSerial.BluetoothSerialPort();

  btSerial.connect(destBtAddress, destBtChannel, () => {
    console.log('connected to bluetooth device');
    sendConnectionState('connected');

    btSerial.on('data', (buffer) => {
      const msg = buffer.toString('utf-8').replace(/\s/g, '');
      if (msg.length > 0) {
        sendCommand(msg);
      }
    });
  }, function (err) {
    console.log('cannot connect');
    console.log(err);
    sendConnectionState('error', err);
  });

  btSerial.inquire();
} catch (e) {
  console.log(e);
  sendConnectionState('error', e);
}
