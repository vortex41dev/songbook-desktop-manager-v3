export const SERVER_PORT = 10100;
// export const SERVER_IP = '127.0.0.1';

// let matches = location.href.match(/https?:\/\/([^:/]+)(:[0-9]+)?/);
export const SERVER_IP = "127.0.0.1"; // matches[1];
export const SERVER_ADDRESS = 'ws://' + SERVER_IP + ':' + SERVER_PORT;


export const DISPLAY_MODE_SONGBOOK = 'songbook';
export const DISPLAY_MODE_SLIDESHOW = 'slideshow';


export const MSG_CHANGE_SLIDE = 1;
export const MSG_CHANGE_SONG = 2;
export const MSG_CHANGE_SLIDES_VISIBILITY = 3;

export const MSG_CHANGE_SLIDE_CODE_PREV = 2;
export const MSG_CHANGE_SLIDE_CODE_NEXT = 3;
export const MSG_CHANGE_SLIDE_CODE_RESTART = 4;

export const BLUETOOTH_CONTROLLER_SETTINGS = {
    btDestAddress: "98:D3:31:FC:4B:C9",
    btDestChannel: 1,
    service: "00001101-0000-1000-8000-00805f9b34fb"
};

export const DEFAULT_SONG_SECTION_FONT_SIZE = '4.5vw';
