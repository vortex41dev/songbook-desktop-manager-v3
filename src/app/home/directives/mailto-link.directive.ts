import {Directive, HostListener, Input} from '@angular/core';

import {shell} from 'electron';

@Directive({
    selector: '[appMailtoLink]'
})
export class MailtoLinkDirective {

    constructor() {
    }

    @Input()
    emailAddress: string;

    @HostListener('click')
    openSendEmailWindow() {
        shell.openExternal('mailto:' + this.emailAddress);
    }

}
