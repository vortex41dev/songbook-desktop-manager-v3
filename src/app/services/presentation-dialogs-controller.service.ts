import {EventEmitter, Injectable, Output} from '@angular/core';
import {MatDialog} from "@angular/material";
import {ConfirmDialogServiceService} from "./confirm-dialog-service.service";
import {PresentationEditorComponent} from "../components/presentation-editor/presentation-editor.component";
import {createPresentation, Presentation} from "../../Model/Presentation";
import {LocalPresentationProviderService} from "./data-providers/local/local-presentation-provider.service";

@Injectable()
export class PresentationDialogsControllerService {
    @Output()
    dialogClosed: EventEmitter<any> = new EventEmitter();

    @Output()
    dialogOpened: EventEmitter<any> = new EventEmitter();

    constructor(private dialog: MatDialog,
                private confirmDialogService: ConfirmDialogServiceService,
                private presentationProvider: LocalPresentationProviderService) {
    }

    newPresentationDialog(onSave: (Presentation) => void) {
        const dlg = this.dialog.open(PresentationEditorComponent);
        dlg.afterClosed().subscribe(x => {
            this.dialogClosed.emit(true);
        });
        const presentationEditComponent = dlg.componentInstance;
        presentationEditComponent.presentation = <Presentation>(createPresentation());
        presentationEditComponent.presentationCreated.subscribe(x => {
            onSave(x);
            dlg.close(x);
        });
        this.dialogOpened.emit(true);
    }

    editPresentationDialog(presentation: Presentation, onSave: (Presentation) => void) {
        const dlg = this.dialog.open(PresentationEditorComponent);
        dlg.afterClosed().subscribe(x => {
            this.dialogClosed.emit(true);
        });
        const presentationEditComponent = dlg.componentInstance;
        presentationEditComponent.presentation = presentation;
        presentationEditComponent.presentationChanged.subscribe(x => {
            onSave(x);
            dlg.close(x);
        });
        this.dialogOpened.emit(true);
    }

    deletePresentationDialog(presentation: Presentation, onDeleted: (Presentation) => void) {
        this.dialogOpened.emit(true);
        this.confirmDialogService.createConfirmDialog('Na pewno chcesz usunąć tą prezentację?', () => {
            this.presentationProvider.deletePresentation(presentation)
                .then(() => {
                    onDeleted(presentation);
                });
            this.dialogClosed.emit(true);
        });
    }
}
