import {Injectable} from '@angular/core';
import {SongFormatConverter} from "./song-format-converter";
import {createSong, createSongSection, Song, SongSection} from "../../../Model/Song";
import {Parser} from "xml2js";
import {DEFAULT_SONG_SECTION_FONT_SIZE} from "../../../settings";

@Injectable()
export class OpenSongFormatConverterService implements SongFormatConverter {
    constructor() {
    }

    convertFromFileData(fileContent: string): Promise<Song> {
        return new Promise<Song>((resolve, reject) => {
            const parser = new Parser({trim: true});
            parser.parseString(fileContent, (err, openSongDocument) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(this.createSongFromJsXmlObj(openSongDocument))
            });
        });
    }

    private createSongFromJsXmlObj(openSongDocument: any): Song {
        console.log(openSongDocument);
        const songElement = openSongDocument.song;
        const title: string = songElement.title[0];
        const songLyrics: string = songElement.lyrics[0];

        const song: Song = createSong();
        song.title = title;
        song.sections = this.parseSectionsFromOpenSongFormatLyrics(songLyrics);
        return song;
    }

    private parseSectionsFromOpenSongFormatLyrics(lyrics: string): SongSection[] {
        return lyrics.split(/\[.+\]/)
            .map(s => OpenSongFormatConverterService.trimTextBlock(s))
            .filter(s => s.length > 0)
            .map(s => {
                const section = createSongSection();
                section.text = s;
                section.fontSize = DEFAULT_SONG_SECTION_FONT_SIZE;
                return section;
            });
    }

    private static trimTextBlock(textBlock): string {
        if (textBlock) {
            return textBlock
                .split('\n')
                .map(line => line.trim())
                .filter(l => !!l)
                .join('\n')
                .replace(/^[\n\r]+/, '')
                .replace(/[\n\r]+$/, '');
        }
        return textBlock;
    }
}
