import {Song} from "../../../Model/Song";
export interface SongFormatConverter {
    convertFromFileData(fileContent: string): Promise<Song>;
}
