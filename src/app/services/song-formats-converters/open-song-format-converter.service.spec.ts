import { TestBed, inject } from '@angular/core/testing';

import { OpenSongFormatConverterService } from './open-song-format-converter.service';

describe('OpenSongFormatConverterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenSongFormatConverterService]
    });
  });

  it('should be created', inject([OpenSongFormatConverterService], (service: OpenSongFormatConverterService) => {
    expect(service).toBeTruthy();
  }));
});
