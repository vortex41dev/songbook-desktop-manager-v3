import {Injectable} from '@angular/core';
import {Presentation, PresentationRepresentation} from "../../../Model/Presentation";
import {
  ImageSlideGroup, ImageSlideGroupBaseRepresentation,
  SLIDE_GROUP_TYPE_IMAGE,
  SLIDE_GROUP_TYPE_SONG,
  SlideGroup,
  SlideGroupBaseRepresentation,
  SongSlideGroup, SongSlideGroupBaseRepresentation
} from "../../../Model/SlideGroup";

@Injectable({
  providedIn: 'root'
})
export class PresentationBuilderService {

  constructor() {
  }

  public buildPresentationFromRepresentation(pres: PresentationRepresentation): Presentation {
    const p = new Presentation();
    p.id = pres.id;
    p.name = pres.name;
    p.slideGroups = pres.slideGroups.map(s => PresentationBuilderService.buildSlideGroup(s)).filter(x => x);

    return p;
  }

  private static buildSlideGroup(s: SlideGroupBaseRepresentation): SlideGroup | null {
    switch (s.type) {
      case SLIDE_GROUP_TYPE_SONG:
        return SongSlideGroup.buildFromSlideGroupRepresentation(s as SongSlideGroupBaseRepresentation);
      case SLIDE_GROUP_TYPE_IMAGE:
        return ImageSlideGroup.buildFromSlideGroupRepresentation(s as ImageSlideGroupBaseRepresentation);
    }
    return null;
  }
}
