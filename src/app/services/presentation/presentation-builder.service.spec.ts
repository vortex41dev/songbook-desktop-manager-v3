import { TestBed } from '@angular/core/testing';

import { PresentationBuilderService } from './presentation-builder.service';

describe('PresentationBuilderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PresentationBuilderService = TestBed.get(PresentationBuilderService);
    expect(service).toBeTruthy();
  });
});
