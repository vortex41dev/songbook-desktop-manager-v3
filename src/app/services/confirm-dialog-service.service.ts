import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from "@angular/material";
import {ConfirmDialogComponent} from "../components/dialog/confirm-dialog/confirm-dialog.component";

@Injectable()
export class ConfirmDialogServiceService {

    constructor(private dialog: MatDialog) {

    }

    createConfirmDialog(text: string,
                        onOk: () => void = () => {},
                        onCancel: () => void = () => {}): MatDialogRef<ConfirmDialogComponent> {
        const dlg = this.dialog.open(ConfirmDialogComponent);
        const confirmComponent = dlg.componentInstance;
        confirmComponent.question = text;
        confirmComponent.onOk.subscribe(x => {
            onOk();
            dlg.close();
        });
        confirmComponent.onCancel.subscribe(x => {
            onCancel();
            dlg.close();
        });
        return dlg;
    }

}
