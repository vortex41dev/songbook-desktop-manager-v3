import {Injectable, Output, EventEmitter} from '@angular/core';
import {MatDialog} from "@angular/material";
import {ConfirmDialogServiceService} from "./confirm-dialog-service.service";
import {SongEditComponent} from "../components/song-edit/song-edit.component";
import {createSong, Song} from "../../Model/Song";

@Injectable()
export class SongEditControllerTraitService {

    constructor(private dialog: MatDialog,
                private confirmDialogService: ConfirmDialogServiceService) {
    }

    @Output()
    dialogClosed: EventEmitter<any> = new EventEmitter();

    @Output()
    dialogOpened: EventEmitter<any> = new EventEmitter();

    createNewDialog(onSave: (Song) => void) {
        const dlg = this.dialog.open(SongEditComponent);
        dlg.afterClosed().subscribe(x => {
            this.dialogClosed.emit(true);
        });
        const songEditComponent = dlg.componentInstance;
        songEditComponent.song = <Song>(createSong());
        songEditComponent.songCreated.subscribe(x => {
            onSave(x);
            dlg.close(x);
        });
        this.dialogOpened.emit(true);
    }

    editSongDialog(song: Song, onSave: (Song) => void) {
        const dlg = this.dialog.open(SongEditComponent);
        dlg.afterClosed().subscribe(x => {
            this.dialogClosed.emit(true);
        });
        const songEditComponent = dlg.componentInstance;
        songEditComponent.song = song;
        songEditComponent.songChanged.subscribe(x => {
            onSave(x);
            dlg.close(song);
        });
        this.dialogOpened.emit(true);
    }

    deleteSongDialog(song: Song, onDeleted: (Song) => void) {
        this.dialogOpened.emit(true);
        this.confirmDialogService.createConfirmDialog('Na pewno chcesz usunąć ten utwór?', () => {
            onDeleted(song);
            this.dialogClosed.emit(true);
        });
    }
}
