import {Injectable, Output, EventEmitter, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {Message} from "../../Model/Message";
import {WebSocketService} from "./websocket.service";
import {
  MSG_CHANGE_SLIDE, MSG_CHANGE_SLIDE_CODE_NEXT, MSG_CHANGE_SLIDE_CODE_PREV, MSG_CHANGE_SONG,
  SERVER_ADDRESS, MSG_CHANGE_SLIDES_VISIBILITY, MSG_CHANGE_SLIDE_CODE_RESTART, MSG_CHANGE_IMAGE,
} from "../../settings";
import {Song} from "../../Model/Song";
import {map} from "rxjs/operators";
import {
  ImageSlideGroup,
  SLIDE_GROUP_TYPE_IMAGE,
  SLIDE_GROUP_TYPE_SONG,
  SlideGroup,
  SongSlideGroup
} from "../../Model/SlideGroup";
import {LocalSongsProviderService} from "./data-providers/local/local-songs-provider.service";
import {ResourceFileSystemService} from "./resource-file-system/resource-file-system.service";

@Injectable()
export class SlideshowControllerService {

  @Output()
  slideChange = new EventEmitter();

  @Output()
  songChange = new EventEmitter();

  @Output()
  slideVisibility = new EventEmitter();

  public serverConnection: Subject<Message> = null;

  constructor(public websockService: WebSocketService, private songsProviderService: LocalSongsProviderService, private resourceFileSystemService: ResourceFileSystemService) {
  }

  connect(slideShowId: string) {
    this.serverConnection = <Subject<Message>>this.websockService.connect(SERVER_ADDRESS) //
      .pipe(
        map((response: MessageEvent): Message => { //
          const data = JSON.parse(response.data);
          return new Message(data.messageType, data.data);
        })
      );
    this.serverConnection.subscribe((message) => {
        this.handleMessage(message);
      },
      (error) => {
        console.error(error);
      }
    );
  }


  sendPrevSlide() {
    const msg = new Message(MSG_CHANGE_SLIDE, {'dir': MSG_CHANGE_SLIDE_CODE_PREV});
    this.serverConnection.next(msg);
  }

  sendNextSlide() {
    const msg = new Message(MSG_CHANGE_SLIDE, {'dir': MSG_CHANGE_SLIDE_CODE_NEXT});
    this.serverConnection.next(msg);
  }

  sendChangeSong(tone: string, song: Song = null) {
    const msg = new Message(MSG_CHANGE_SONG, {'id': song.id, 'tone': tone, song: song});
    this.serverConnection.next(msg);
  }

  sendSlideVisibility(visibility: boolean) {
    const msg = new Message(MSG_CHANGE_SLIDES_VISIBILITY, visibility);
    this.serverConnection.next(msg);
  }

  sendRestartSlideShow() {
    const msg = new Message(MSG_CHANGE_SLIDE, {'dir': MSG_CHANGE_SLIDE_CODE_RESTART});
    this.serverConnection.next(msg);
  }

  private handleMessage(message: Message) {
    switch (message.messageType) {
      case MSG_CHANGE_SLIDE:
        this.slideChange.emit(message.data);
        break;
      case MSG_CHANGE_SONG:
        this.songChange.emit(message.data);
        break;
      case MSG_CHANGE_SLIDES_VISIBILITY:
        this.slideVisibility.emit(message.data);
        break;
    }
  }

  sendChangeSlideGroup(slideGroup: SlideGroup) {
    if (slideGroup.type === SLIDE_GROUP_TYPE_IMAGE) {
      this.sendImage(slideGroup as ImageSlideGroup);
    }
    if (slideGroup.type === SLIDE_GROUP_TYPE_SONG) {
      const songSlideGroup = slideGroup as SongSlideGroup;
      this.songsProviderService.getSong(songSlideGroup.songId, songSlideGroup.keyTone)
        .then(song => {
          if (!song) {
            return;
          }
          this.sendChangeSong(songSlideGroup.keyTone, song);
        });
    }
  }

  private sendImage(imageSlideGroup: ImageSlideGroup) {
    const msg = new Message(MSG_CHANGE_IMAGE, {
      'id': imageSlideGroup.getId(),
      'name': imageSlideGroup.name,
      'path': this.resourceFileSystemService.getPathForResource(imageSlideGroup.image)
    });
    console.log(msg);
    this.serverConnection.next(msg);
  }
}
