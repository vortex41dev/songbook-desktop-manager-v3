import {Injectable} from '@angular/core';
import {IndexedDB} from "../../indexeddb";

@Injectable()
export class LocalDatabaseService {

  private static database: IndexedDB = new IndexedDB("songbook", 3);

  constructor() {
  }

  static getDb(): IndexedDB {
    return LocalDatabaseService.database;
  }

  public static setupStore(setupIndexesCallbacks: ((a: any) => void)[]): Promise<any> {
    return LocalDatabaseService.getDb().createStore(5, (e) => {
      setupIndexesCallbacks.map(callback => callback(e));
    });
  }

}
