import {LocalDatabaseService} from "../local-database.service";

export class IndexedDbDataProviderService {

  protected static registeredProviders = [];

  public static registerProvider(provider: any) {
    this.registeredProviders.push(provider);
  }

  // must be declared inside method, so it wont be optimized on compile time
  // this way is needed to prevent from circular dependency that typescript 2.3.3 has problems with
  private static getRegisteredProviders() {
    return this.registeredProviders;
  }

  protected static setupStore(): Promise<any> {
    return LocalDatabaseService.setupStore(
      IndexedDbDataProviderService.getRegisteredProviders().map(x => x.setupStoreIndex)
    );
  }

  /**
   * Returns promise resulting in json object containing all stores saved
   * in the indexedDb and registers in IndexedDbDataProviderService
   *
   * @returns {Promise<any>}
   */
  public static exportStore(): Promise<any> {
    return Promise.all(
      IndexedDbDataProviderService.getRegisteredProviders()
        .map(provider => LocalDatabaseService.getDb()
          .getAll(provider.getIndexName()).then(records => {
            return {name: provider.getIndexName(), data: records}
          })
        )).then(nameDataObjects => {
      const resultObject = {};
      for (const nameObjRec of nameDataObjects) {
        resultObject[nameObjRec.name] = nameObjRec.data;
      }
      return resultObject;
    });
  }


  public static importStore(data: any, clearDb: boolean = false): Promise<any> {
    const importDataLambda = () => Promise.all(IndexedDbDataProviderService.getRegisteredProviders().map(p => {
      const indexStoreDataToImport = data[p.getIndexName()];
      return Promise.all(indexStoreDataToImport
        .map(element => LocalDatabaseService.getDb().add(p.getIndexName(), element)));
    }));
    if (clearDb) {
      return IndexedDbDataProviderService.clearStore().then(importDataLambda);
    } else {
      return importDataLambda();
    }
  }

  public static clearStore(): Promise<any> {
    return Promise.all(IndexedDbDataProviderService.getRegisteredProviders().map(p =>
      LocalDatabaseService.getDb().clear(p.getIndexName())));
  }

  public static makeSureStoreCreated<T>(dataRequest: () => Promise<T>) {
    return new Promise<T>((resolve, reject) => {
      IndexedDbDataProviderService.setupStore().then((store) => {
        dataRequest().then(resolve).catch(reject);
      }).catch(reject);
    });
  }

  public static makeSureStoreCreatedPromise(): Promise<any> {
    return IndexedDbDataProviderService.setupStore();
  }
}
