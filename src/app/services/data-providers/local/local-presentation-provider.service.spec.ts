import {TestBed, inject} from '@angular/core/testing';

import {LocalPresentationProviderService} from './local-presentation-provider.service';

describe('LocalPresentationProviderService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LocalPresentationProviderService]
        });
    });

    it('should be created', inject([LocalPresentationProviderService], (service: LocalPresentationProviderService) => {
        expect(service).toBeTruthy();
    }));
});
