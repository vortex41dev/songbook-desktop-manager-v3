import {Injectable} from '@angular/core';
import {IndexedDbDataProviderService} from "./abstract/indexed-db-data-provider.service";
import {saveAs} from 'file-saver';

@Injectable()
export class IndexedDbManagerService {

    public exportStorage(): Promise<any> {
        return IndexedDbDataProviderService.exportStore().then(data => {
            const file = new Blob([JSON.stringify(data)], {type: 'application/json;charset=utf-8'});
            const date = new Date();
            saveAs(file, 'Duch-songbook-db_' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString().replace(/:/, '.') + '.json');
            return file;
        });
    }

    public importStorage(data: any, clearDb: boolean = false): Promise<any> {
        return IndexedDbDataProviderService.importStore(data, clearDb);
    }

    public clearStorage(): Promise<any> {
        return IndexedDbDataProviderService.clearStore();
    }

    constructor() {
    }

}
