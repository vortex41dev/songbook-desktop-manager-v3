import {EventEmitter, Injectable} from '@angular/core';
import {IndexedDbDataProviderService} from "./abstract/indexed-db-data-provider.service";
import {PresentationProvider} from "../abstract/presentation-provider";
import {Presentation, PresentationRepresentation} from "../../../../Model/Presentation";
import {LocalDatabaseService} from "./local-database.service";
import {UUID} from "angular2-uuid";
import {PresentationBuilderService} from "../../presentation/presentation-builder.service";


export const PRESENTATIONS_STORE_NAME = 'presentations';

@Injectable()
export class LocalPresentationProviderService implements PresentationProvider {

  listChanged: EventEmitter<any> = new EventEmitter<any>();
  changed: EventEmitter<Presentation> = new EventEmitter<Presentation>();
  deleted: EventEmitter<Presentation> = new EventEmitter<Presentation>();
  created: EventEmitter<Presentation> = new EventEmitter<Presentation>();

  constructor(protected presentationBuilderService: PresentationBuilderService) {
  }

  public static getIndexName(): string {
    return PRESENTATIONS_STORE_NAME;
  }

  public static setupStoreIndex(evt: any) {
    const objectStore = evt.currentTarget.result.createObjectStore(
      PRESENTATIONS_STORE_NAME, {keyPath: "id", autoIncrement: false});

    objectStore.createIndex("name", "name", {unique: false});
    objectStore.createIndex("slideGroups", "slideGroups", {unique: false});
    return objectStore;
  };

  getPresentations(): Promise<Presentation[]> {
    return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
      .getDb()
      .getAll(PRESENTATIONS_STORE_NAME)
      .then(presList => {
        return presList
          .map(p => this.presentationBuilderService.buildPresentationFromRepresentation(p as PresentationRepresentation));
      })
    );
  }

  saveNewPresentation(presentation: Presentation) {
    presentation.id = UUID.UUID();
    return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
      .getDb().add(PRESENTATIONS_STORE_NAME, presentation).then(p => {
        this.listChanged.emit(true);
        this.created.emit(p);
        return p;
      }));
  }

  savePresentation(presentation: Presentation): Promise<Presentation> {
    return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
      .getDb().update(PRESENTATIONS_STORE_NAME, presentation).then(p => {
        this.changed.emit(p);
        return p;
      }));
  }

  deletePresentation(presentation: Presentation): Promise<any> {
    return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
      .getDb().delete(PRESENTATIONS_STORE_NAME, presentation.id).then(p => {
        this.deleted.emit(p);
        this.listChanged.emit(true);
        return p;
      }));
  }

}

IndexedDbDataProviderService.registerProvider(LocalPresentationProviderService);
