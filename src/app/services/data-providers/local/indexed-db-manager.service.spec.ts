import { TestBed, inject } from '@angular/core/testing';

import { IndexedDbManagerService } from './indexed-db-manager.service';

describe('IndexedDbManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IndexedDbManagerService]
    });
  });

  it('should be created', inject([IndexedDbManagerService], (service: IndexedDbManagerService) => {
    expect(service).toBeTruthy();
  }));
});
