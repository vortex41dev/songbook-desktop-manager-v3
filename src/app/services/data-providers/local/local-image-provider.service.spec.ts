import { TestBed, inject } from '@angular/core/testing';

import { LocalImageProviderService } from './local-resource-manager.service';

describe('LocalImageProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalImageProviderService]
    });
  });

  it('should be created', inject([LocalImageProviderService], (service: LocalImageProviderService) => {
    expect(service).toBeTruthy();
  }));
});
