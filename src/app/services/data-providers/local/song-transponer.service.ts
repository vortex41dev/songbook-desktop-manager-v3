import {Injectable} from '@angular/core';
import {Song, SongSection} from "../../../../Model/Song";

const LINE_DELIMITER = "\n";
const CHORD_SPLITTER = " ";
const CHROMATIC_SCALES = [
    ['a', 'b', 'h', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#'],
    ['A', 'B', 'H', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'],
];

@Injectable()
export class SongTransponerService {

    public static assertValidTone(tone) {
        if (CHROMATIC_SCALES[0].indexOf(tone.toLowerCase()) === -1) {
            throw new Error("error.tone.invalid");
        }
    }

    public static assertSameChromaticScale(tone1, tone2) {
        SongTransponerService.assertValidTone(tone1);
        SongTransponerService.assertValidTone(tone2);
        // if ((SongTransponerService.isUppercase(tone1[0]) ^ SongTransponerService.isUppercase(tone2[0])) !== 0) {
            // throw new Error("error.tone.scale_not_match");
        // }
    }

    private static isUppercase(text: string): number {
        return text === text.toUpperCase() ? 1 : 0;
    }

    public transposeSong(song: Song, firstTone) {
        const songStartupTone = SongTransponerService.getFirstToneOfSong(song);

        if (!songStartupTone) {
            return song;
        } else {
            SongTransponerService.assertSameChromaticScale(songStartupTone, firstTone);
        }
        const interval = SongTransponerService.getIntervalBetween(songStartupTone, firstTone);
        for (const section of song.sections) {
            this.transposeSectionByInterval(section, interval);
        }
        return song;
    }

    public transposeSection(section: SongSection, firstTone) {
        // @todo add assert

        const chordLines = SongTransponerService.extractChordsFromText(section.chords);
        const sectionFirstTone = SongTransponerService.getFirstTone(chordLines);
        if (!sectionFirstTone) {
            return section;
        } else {
            SongTransponerService.assertSameChromaticScale(sectionFirstTone, firstTone);
        }
        const interval = SongTransponerService.getIntervalBetween(sectionFirstTone, firstTone);
        return this.transposeSectionByInterval(section, interval);
    }

    private transposeSectionByInterval(section: SongSection, interval) {
        const chordLines = SongTransponerService.extractChordsFromText(section.chords);
        const chordsTransposed = this.transposeChordsByInterval(chordLines, interval);
        section.chords = this.joinChordsArrIntoText(chordsTransposed);
        return section;
    }

    /**
     * @return string|null
     * @param lines
     */
    private static getFirstTone(lines: string[][]) {
        for (const line of lines) {
            for (const chord of line) {
                if (chord) {
                    return SongTransponerService.extractToneFromChord(chord);
                }
            }
        }
        return null;
    }

    /**
     * @return array
     * @param chordsText
     */
    private static extractChordsFromText(chordsText: string = null) {
        if (!chordsText) {
            return [];
        }
        const lines = chordsText.split(LINE_DELIMITER);
        return lines.map(line => {
            return line.split(' ').map(t => t.trim()).filter(x => x);
        });
    }

    /**
     * @return mixed|null
     * @param sectionFirstTone
     * @param firstTone
     */
    public static getIntervalBetween(sectionFirstTone, firstTone) {
        const tonnationType = SongTransponerService.isUppercase(sectionFirstTone[0]) ? 1 : 0;
        const tonnation = CHROMATIC_SCALES[tonnationType];
        const sectionFirstToneIndex = tonnation.indexOf(sectionFirstTone);
        const firstToneIndex = tonnation.indexOf(firstTone);
        return sectionFirstToneIndex !== -1 && firstToneIndex !== -1 ? firstToneIndex - sectionFirstToneIndex : null;
    }

    /**
     * @return array
     * @param chordLines
     * @param interval
     */
    private transposeChordsByInterval(chordLines, interval) {
        return chordLines.map(chordLine => {
            return chordLine.map(chord => SongTransponerService.transposeChord(chord, interval));
        });
    }

    /**
     * @return string
     * @param chord
     * @param interval
     */
    public static transposeChord(chord, interval) {
        const [tone, rest] = SongTransponerService.splitChordIntoBareToneAndRest(chord);
        return SongTransponerService.shiftToneByInterval(tone, interval) + rest;
    }

    /**
     * @return array
     * @param chord
     */
    private static splitChordIntoBareToneAndRest(chord) {
        if (!chord) {
            return [null, null];
        }
        const hasSharp = chord.length > 1 && chord[1] === '#';
        const tone = chord[0] + (hasSharp ? '#' : '');
        const rest = chord.substr(hasSharp ? 2 : 1);
        return [tone, rest];
    }

    /**
     * @return null
     * @param tone
     * @param interval
     */
    private static shiftToneByInterval(tone, interval) {
        const tonnationType = SongTransponerService.isUppercase(tone[0]) ? 1 : 0;
        const tonnation = CHROMATIC_SCALES[tonnationType];
        const toneIndex = tonnation.indexOf(tone);
        if (toneIndex === -1) {
            return null;
        }
        let transposedIndex = (toneIndex + interval) % tonnation.length;
        if (transposedIndex < 0) {
            transposedIndex += tonnation.length;
        }
        return tonnation[transposedIndex];
    }

    /**
     * @return string
     * @param chord
     */
    private static extractToneFromChord(chord) {
        const hasSharp = chord.length > 1 && chord[1] === '#';
        return chord.substr(0, hasSharp ? 2 : 1);
    }

    /**
     * @return string
     * @param chordsTransposed
     */
    private joinChordsArrIntoText(chordsTransposed) {
        return chordsTransposed.map(line => line.join(CHORD_SPLITTER)).join(LINE_DELIMITER);
    }

    private static getFirstToneOfSong(song: Song) {
        for (const section of song.sections) {
            const lines = SongTransponerService.extractChordsFromText(section.chords);
            const firstTone = SongTransponerService.getFirstTone(lines);
            if (firstTone) {
                return firstTone;
            }
        }
        return null;
    }

    public static getSongKeyTone(song: Song): string {
        if (!song) {
            return null;
        }
        for (const section of song.sections) {
            if (!section.chords) {
                continue;
            }
            const chords = section.chords;
            const tones = chords
                .split(/\s+/)
                .map(chord => {
                    return chord[0] + (chord.length > 1 && chord[1] === '#' ? '#' : '');
                });
            if (tones.length > 0) {
                return tones[0];
            }
        }
        return null;
    }
}
