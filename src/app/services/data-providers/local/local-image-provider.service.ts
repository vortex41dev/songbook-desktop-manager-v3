import {Injectable} from '@angular/core';
import {ImageProvider} from "../abstract/image-provider";
import {ImageSlideGroup} from "../../../../Model/SlideGroup";
import {IndexedDbDataProviderService} from "./abstract/indexed-db-data-provider.service";
import {LocalDatabaseService} from "./local-database.service";
import {BinaryFileReader} from "../../../utils/text-file-reader";

const IMAGES_STORE_NAME = 'images';

export const RESOURCES_PATH = process.cwd() + '/resources/';

@Injectable()
export class LocalImageProviderService implements ImageProvider {

  private static fs = require('fs');

  public static getIndexName(): string {
    return IMAGES_STORE_NAME;
  }

  public static setupStoreIndex(evt: any) {
    console.log(evt);
    const objectStore = evt.currentTarget.result.createObjectStore(
      IMAGES_STORE_NAME, {keyPath: "id", autoIncrement: false});

    objectStore.createIndex("name", "name", {unique: false});
    objectStore.createIndex("image", "image", {unique: false});
    return objectStore;
  };

  getImage(id: string): Promise<ImageSlideGroup> {
    return IndexedDbDataProviderService.makeSureStoreCreated(() =>
      LocalDatabaseService.getDb()
        .getByKey(IMAGES_STORE_NAME, id)
        .then(value => LocalImageProviderService.createImageSlideGroupFromRepresentation(value))
    );
  }

  private static createImageSlideGroupFromRepresentation(value): ImageSlideGroup {
    return new ImageSlideGroup(
      value.id, value.name, value.image
    );
  }

  saveImage(id: any, name: string, image: Blob): Promise<ImageSlideGroup> {
    console.log(image);
    return new Promise<ImageSlideGroup>((resolve, reject) => {
      BinaryFileReader.readFileAsArrayBuffer(image).then(data => {
        console.log(data);
        LocalImageProviderService.fs.writeFile(RESOURCES_PATH + name, Buffer.from(data), () => {
          const imageSlideGroup = new ImageSlideGroup(id, name, name);

          IndexedDbDataProviderService.makeSureStoreCreatedPromise()
            .then(() => LocalDatabaseService
              .getDb()
              .update(IMAGES_STORE_NAME, imageSlideGroup)
              .then(s => {
                resolve(s);
                return s;
              }).catch(reason => reject(reason))
            );
        });
      })
    });
  }

  removeImage(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getImage(id).then(img => {
        LocalImageProviderService.fs.unlink(RESOURCES_PATH + img.image, () => {
          IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
            .getDb().delete(IMAGES_STORE_NAME, img.id).then(s => {
              resolve(true);
            })
          );
        });
      });
    });
  }

  getAllImages(): Promise<ImageSlideGroup[]> {
    return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
      .getDb()
      .getAll(IMAGES_STORE_NAME)
      .then(presList => {
        console.log(presList);
        return presList.map(p => LocalImageProviderService.createImageSlideGroupFromRepresentation(p));
      })
    );
  }

}

IndexedDbDataProviderService.registerProvider(LocalImageProviderService);
