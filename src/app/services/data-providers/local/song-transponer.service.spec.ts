import {TestBed, inject} from '@angular/core/testing';

import {SongTransponerService} from './song-transponer.service';
import {Song, SongSection} from "../../../../Model/Song";

describe('SongTransponerService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SongTransponerService]
        });
    });

    it('should be created', inject([SongTransponerService], (service: SongTransponerService) => {
        expect(service).toBeTruthy();
    }));

    it('Tone should be transposed', inject([SongTransponerService], (service: SongTransponerService) => {
        expect(SongTransponerService.transposeChord('d', 2)).toEqual('e');
    }));

    it('Section should be transposed', inject([SongTransponerService], (service: SongTransponerService) => {
        const songSection = <SongSection>{
            id: '0',
            text: '',
            chords: 'A D e F#',
            fontSize: '4.5'
        };
        expect(service.transposeSection(songSection, 'C').chords).toEqual('C F g A');
    }));

    it('Song should be transposed', inject([SongTransponerService], (service: SongTransponerService) => {
        const songSections = [
            <SongSection>{
                id: '0',
                text: '',
                chords: 'A D e F#',
                fontSize: '4.5'
            },
            <SongSection>{
                id: '0',
                text: '',
                chords: 'h d e C#',
                fontSize: '4.5'
            }];
        const song = <Song>{
            id: '0',
            categories: [],
            title: '',
            order: 0,
            sections: songSections
        };
        service.transposeSong(song, 'C');
        expect(song.sections[0].chords).toEqual('C F g A');
        expect(song.sections[1].chords).toEqual('d f g E');
    }));

});
