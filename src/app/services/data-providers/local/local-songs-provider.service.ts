import {EventEmitter, Injectable} from '@angular/core';
import {SongProvider} from "../abstract/song-provider";
import {Song} from "../../../../Model/Song";
import {LocalDatabaseService} from "./local-database.service";
import {SongTransponerService} from "./song-transponer.service";
import {IndexedDbDataProviderService} from "./abstract/indexed-db-data-provider.service";
import {UUID} from "angular2-uuid";

export const SONGS_STORE_NAME = 'songs';

@Injectable()
export class LocalSongsProviderService implements SongProvider {

    public static getIndexName(): string {
        return SONGS_STORE_NAME;
    }

    public static setupStoreIndex(evt: any) {
        const objectStore = evt.currentTarget.result.createObjectStore(
            SONGS_STORE_NAME, {keyPath: "id", autoIncrement: false});

        objectStore.createIndex("title", "title", {unique: false});
        objectStore.createIndex("order", "order", {unique: false});
        objectStore.createIndex("categories", "categories", {unique: false});
        objectStore.createIndex("sections", "sections", {unique: false});
        return objectStore;
    };

    getSong(id: string, keyTone: string): Promise<Song> {
        return IndexedDbDataProviderService.makeSureStoreCreated(() =>
            LocalDatabaseService.getDb().getByKey(SONGS_STORE_NAME, id).then(song => this.songTransposer.transposeSong(song, keyTone)));
    }

    getSongsList(): Promise<Song[]> {
        return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
            .getDb().getAll(SONGS_STORE_NAME));
    }

    saveSong(song: Song): Promise<Song> {
        return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
            .getDb().update(SONGS_STORE_NAME, song).then(s => {
                this.changed.emit(s);
                return s;
            }));
    }

    saveSongAsNew(song: Song): Promise<Song> {
        song.id = UUID.UUID();
        return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
            .getDb().add(SONGS_STORE_NAME, song).then(s => {
                this.listChanged.emit(true);
                this.created.emit(s);
                return s;
            }));
    }

    deleteSong(song: Song): Promise<any> {
        return IndexedDbDataProviderService.makeSureStoreCreated(() => LocalDatabaseService
            .getDb().delete(SONGS_STORE_NAME, song.id).then(s => {
                this.listChanged.emit(true);
                this.deleted.emit(s);
            }));
    }

    listChanged: EventEmitter<any> = new EventEmitter<any>();
    changed: EventEmitter<Song> = new EventEmitter<Song>();
    deleted: EventEmitter<Song> = new EventEmitter<Song>();
    created: EventEmitter<Song> = new EventEmitter<Song>();

    constructor(private songTransposer: SongTransponerService) {
    }

}

IndexedDbDataProviderService.registerProvider(LocalSongsProviderService);
