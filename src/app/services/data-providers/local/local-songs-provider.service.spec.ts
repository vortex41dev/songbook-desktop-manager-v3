import {TestBed, inject} from '@angular/core/testing';

import {LocalSongsProviderService} from './local-songs-provider.service';
import {LocalDatabaseService} from "./local-database.service";

describe('LocalSongsProviderService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LocalSongsProviderService, LocalDatabaseService]
        });
    });

    it('should be created', inject([LocalSongsProviderService], (service: LocalSongsProviderService) => {
        expect(service).toBeTruthy();
    }));
});
