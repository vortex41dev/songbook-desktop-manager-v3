import {ImageSlideGroup} from "../../../../Model/SlideGroup";
import {Injectable} from "@angular/core";

@Injectable()
export abstract class ImageProvider {
  abstract getImage(id: string): Promise<ImageSlideGroup> ;

  abstract getAllImages(): Promise<ImageSlideGroup[]>;

  abstract saveImage(id, name: string, image: Blob): Promise<ImageSlideGroup>;

  abstract removeImage(id): Promise<any>;
}
