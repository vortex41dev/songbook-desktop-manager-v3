import {Song} from "../../../../Model/Song";
import {EventEmitter} from "@angular/core";

export interface SongProvider {
    getSong(id: string, keyTone: string): Promise<Song>;

    getSongsList(): Promise<Song[]>;

    saveSong(song: Song): Promise<Song>;

    saveSongAsNew(song: Song): Promise<Song>;

    deleteSong(song: Song): Promise<any>;

    listChanged: EventEmitter<any>;
    changed: EventEmitter<Song>;
    deleted: EventEmitter<Song>;
    created: EventEmitter<Song>;
}
