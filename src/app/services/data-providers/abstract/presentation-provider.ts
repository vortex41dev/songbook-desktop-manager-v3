import {EventEmitter} from "@angular/core";
import {Presentation} from "../../../../Model/Presentation";

export interface PresentationProvider {

    getPresentations(): Promise<Presentation[]>;

    saveNewPresentation(presentation: Presentation) ;

    savePresentation(presentation: Presentation): Promise<Presentation>;


    deletePresentation(presentation: Presentation): Promise<any>;

    listChanged: EventEmitter<any>;
    changed: EventEmitter<Presentation>;
    deleted: EventEmitter<Presentation>;
    created: EventEmitter<Presentation>;
}
