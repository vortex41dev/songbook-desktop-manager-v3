import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {SERVER_IP} from "../../../../settings";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";


const API_PATH = "http://" + SERVER_IP + "/songbook-api/";

@Injectable()
export class RestProvider {

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(protected http: HttpClient) {
  }

  get<ResponseType>(url: string): Promise<ResponseType> {
    return this.http.get(API_PATH + url)
      .pipe(
        map(res => <ResponseType>res)
      ).toPromise();
  }

  postJson(url: string, data: any): Observable<any> {
    return this.http.post(
      API_PATH + url,
      JSON.stringify(data),
      {headers: this.headers}
    )
  }

  putJson(url: string, data: any): Observable<any> {
    return this.http.put(
      API_PATH + url,
      JSON.stringify(data),
      {headers: this.headers}
    )
  }

  post<ResponseType>(url: string, data: any): Promise<ResponseType> {
    return this.postJson(url, data)
      .pipe(
        map(res => <ResponseType>res)
      ).toPromise();
  }

  put<ResponseType>(url: string, data: any): Promise<ResponseType> {
    return this.putJson(url, data)
      .pipe(
        map(res => <ResponseType>res)
      ).toPromise();
  }

  delete(url: string): Promise<any> {
    return this.http.delete(API_PATH + url)
      .pipe(
        map(res => res)
      ).toPromise();
  }
}
