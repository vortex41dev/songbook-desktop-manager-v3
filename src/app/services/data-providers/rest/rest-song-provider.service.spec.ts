import {TestBed, inject} from '@angular/core/testing';
import {RestSongProvider} from "./rest-song-provider.service";
import {HttpModule} from "@angular/http";

describe('RestSongProvider', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [RestSongProvider],
            imports: [HttpModule],
        });
    });

    it('should ...', inject([RestSongProvider], (service: RestSongProvider) => {
        expect(service).toBeTruthy();
    }));
});
