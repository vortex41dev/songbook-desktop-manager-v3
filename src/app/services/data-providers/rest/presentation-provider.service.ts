import {Injectable} from '@angular/core';
import {RestProvider} from "./rest-provider.service";
import {Presentation} from "../../../../Model/Presentation";

const PRESENTATIONS_PATH = 'presentations';

@Injectable()
export class PresentationProviderService extends RestProvider {

    getPresentations(): Promise<Presentation[]> {
        return this.get<Presentation[]>(PRESENTATIONS_PATH);
    }

    saveNewPresentation(presentation: Presentation) {
        return this.post<Presentation>(PRESENTATIONS_PATH, {
            name: presentation.name,
            slideGroups: presentation.slideGroups
        });
    }

    savePresentation(presentation: Presentation) {
        return this.put(PresentationProviderService.getPresentationUrl(presentation), {
            id: presentation.id,
            name: presentation.name,
            slideGroups: presentation.slideGroups
        });
    }


    deletePresentation(presentation: Presentation): Promise<any> {
        return this.delete(PresentationProviderService.getPresentationUrl(presentation));
    }

    private static getPresentationUrl(presentation: Presentation) {
        return PRESENTATIONS_PATH + '/' + presentation.id;
    }
}
