import {EventEmitter, Injectable} from '@angular/core';
import {RestProvider} from "./rest-provider.service";
import {SongProvider} from "../abstract/song-provider";
import {Song, SongSection} from "../../../../Model/Song";

const SONGS_PATH = 'songs';

@Injectable()
export class RestSongProvider extends RestProvider implements SongProvider {
    listChanged: EventEmitter<any> = new EventEmitter<any>();
    changed: EventEmitter<Song> = new EventEmitter<Song>();
    deleted: EventEmitter<Song> = new EventEmitter<Song>();
    created: EventEmitter<Song> = new EventEmitter<Song>();

    getSong(id: string, keyTone: string): Promise<Song> {
        return this.get<Song>(SONGS_PATH + '/' + id + (keyTone ? '?tone=' + keyTone.replace('#', 'i') : ''));
    }

    getSongsList(): Promise<Song[]> {
        return this.get<Song[]>(SONGS_PATH);
    }

    saveSong(song: Song): Promise<Song> {
        return this.put<Song>(SONGS_PATH + '/' + song.id, {
            id: song.id,
            order: song.order,
            title: song.title,
            sections: RestSongProvider.serializeSections(song.sections),
        });
    }

    saveSongAsNew(song: Song): Promise<Song> {
        return this.post<Song>(SONGS_PATH, {
            order: song.order,
            title: song.title,
            sections: RestSongProvider.serializeSections(song.sections),
        });
    }

    deleteSong(song: Song): Promise<any> {
        return this.delete(SONGS_PATH + '/' + song.id);
    }

    private static serializeSections(sections: SongSection[]) {
        return sections.map(s => {
            return {
                text: s.text,
                chords: s.chords,
                fontSize: s.fontSize,
            };
        });
    }
}
