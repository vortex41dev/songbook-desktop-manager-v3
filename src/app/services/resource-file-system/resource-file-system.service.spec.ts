import { TestBed } from '@angular/core/testing';

import { ResourceFileSystemService } from './resource-file-system.service';

describe('ResourceFileSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResourceFileSystemService = TestBed.get(ResourceFileSystemService);
    expect(service).toBeTruthy();
  });
});
