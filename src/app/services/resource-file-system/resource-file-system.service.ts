import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResourceFileSystemService {

  public getPathForResource(uri: string): string {
    return `http://localhost:10800/resources/${uri}`;
  }
}
