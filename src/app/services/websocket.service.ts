import {EventEmitter, Injectable} from "@angular/core";
import {Subject, Observer, Observable} from "rxjs";

@Injectable()
export class WebSocketService {
    private subject: Subject<MessageEvent>;
    private ws: WebSocket;

    public connectedToServer: EventEmitter<any> = new EventEmitter();

    public connect(url): Subject<MessageEvent> {
        if (!this.subject) {
            this.subject = this.create(url);
        }

        return this.subject;
    }

    public disconnect() {
        if (this.ws) {
            this.ws.close();
            this.ws = null;
            this.subject = null;
        }
    }

    private create(url): Subject<MessageEvent> {

        this.ws = new WebSocket(url);

        const observable = new Observable((obs: Observer<MessageEvent>) => {
            this.ws.onmessage = obs.next.bind(obs);
            this.ws.onerror = obs.error.bind(obs);
            this.ws.onclose = obs.complete.bind(obs);

            return this.ws.close.bind(this.ws);
        });
        this.ws.onopen = () => {
            this.connectedToServer.emit(true);
        };

        const observer = {
            next: (data: Object) => {
                if (this.ws && this.ws.readyState === WebSocket.OPEN) {
                    this.ws.send(JSON.stringify(data));
                }
            },
        };

        return Subject.create(observer, observable);
    }

}
