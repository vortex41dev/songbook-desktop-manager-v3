import {EventEmitter, Injectable} from '@angular/core';
import {ChildProcess, fork, spawn} from "child_process";
import {SIGINT, SIGKILL} from "constants";

@Injectable()
export class ServerProcessService {

  private childProcess: ChildProcess = null;

  public clientListChanged: EventEmitter<any> = new EventEmitter();

  public start(): Promise<any> {
    if (!this.childProcess) {
      return new Promise((resolve, reject) => {
        this.childProcess = fork("server.js", ["10100"]);
        this.childProcess
          .on('message', (response) => {
            console.log("Recvd msg:");
            console.log(response);
            switch (response.messageType) {
              case 'GOT_CLIENT':
                this.childProcess.send({messageType: 'CLIENTS_LIST'});
                break;
              case 'CLIENT_DISCONNECTED':
                this.childProcess.send({messageType: 'CLIENTS_LIST'});
                break;
              case 'CLIENTS_LIST':
                this.clientListChanged.emit(response.data);
                break;
            }
            resolve(this.childProcess.pid);
          })
          .on('error', (err: Error) => {
            reject(err);
          })
          .on('exit', (code, signal) => {
            console.log('SERVER: exit:');
            console.log(code);
            console.log(signal);
          })
          .on('close', (code, signal) => {
            console.log('SERVER: exit:');
            console.log(code);
            console.log(signal);
          });

        this.childProcess.send({messageType: 'HELLO'});
      });
    } else {
      return null;
    }
  }

  public stop() {
    if (this.childProcess) {
      const childProcPid = this.childProcess.pid;
      this.childProcess.kill();
      this.childProcess = null;
      console.log("Process [" + childProcPid + "] has been killed");
    }
  }

}
