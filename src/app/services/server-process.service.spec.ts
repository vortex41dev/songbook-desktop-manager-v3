import { TestBed, inject } from '@angular/core/testing';

import { ServerProcessService } from './server-process.service';

describe('ServerProcessService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerProcessService]
    });
  });

  it('should be created', inject([ServerProcessService], (service: ServerProcessService) => {
    expect(service).toBeTruthy();
  }));
});
