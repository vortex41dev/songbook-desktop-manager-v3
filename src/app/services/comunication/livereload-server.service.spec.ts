import { TestBed, inject } from '@angular/core/testing';

import { LivereloadServerService } from './livereload-server.service';

describe('LivereloadServerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LivereloadServerService]
    });
  });

  it('should be created', inject([LivereloadServerService], (service: LivereloadServerService) => {
    expect(service).toBeTruthy();
  }));
});
