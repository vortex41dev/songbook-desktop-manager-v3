import {EventEmitter, Injectable} from '@angular/core';
import {ChildProcess, exec, fork, spawnSync} from "child_process";


@Injectable()
export class BluetoothControllerService {

  protected static instance: BluetoothControllerService = null;

  // Return the instance of the service
  public static getInstance(): BluetoothControllerService {
    if (BluetoothControllerService.instance === null) {
      BluetoothControllerService.instance = new BluetoothControllerService();
    }
    return BluetoothControllerService.instance;
  }


  public connectionStateChanged: EventEmitter<any> = new EventEmitter();
  public prevSongPressed: EventEmitter<any> = new EventEmitter();
  public nextSongPressed: EventEmitter<any> = new EventEmitter();
  public prevSlidePressed: EventEmitter<any> = new EventEmitter();
  public nextSlidePressed: EventEmitter<any> = new EventEmitter();
  public restartPressed: EventEmitter<any> = new EventEmitter();
  public toggleVisibilityPressed: EventEmitter<any> = new EventEmitter();

  private childProcess: ChildProcess = null;

  start(btDestAddress: string, btDestChannel: string): Promise<any> {
    if (!this.childProcess) {
      return new Promise((resolve, reject) => {
        this.childProcess = fork("btController.js", [btDestAddress, btDestChannel]);
        this.childProcess.on('message', (message) => {
          console.log("Recvd msg:");
          console.log(message);
          switch (message.messageType) {
            case 'state':
              this.connectionStateChanged.emit(message.data);
              break;
            case 'command':
              this.dispatchCommand(message.data);
              break;
            default:
              console.log("WTF message: " + JSON.stringify(message));
          }
          resolve(this.childProcess.pid);
        });
        this.childProcess.on('close', () => {
          this.connectionStateChanged.emit({state: 'disconnected'});
        });
        this.childProcess.send({messageType: 'HELLO'});
      });
    } else {
      return null;
    }

  }

  dispatchCommand(command) {
    switch (command.toString()) {
      case '0':
        this.prevSongPressed.emit(true);
        break;
      case '1':
        this.nextSongPressed.emit(true);
        break;
      case '2':
        this.prevSlidePressed.emit(true);
        break;
      case '3':
        this.nextSlidePressed.emit(true);
        break;
      case '4':
        this.toggleVisibilityPressed.emit(true);
        break;
      case 'e':
        this.restartPressed.emit(true);
        break;
    }
  }

  // close the connection when you're ready
  stop() {
    this.connectionStateChanged.emit({state: 'disconnected'});
    if (this.childProcess) {
      const childProcPid = this.childProcess.pid;
      this.childProcess.kill();
      this.childProcess = null;
      console.log("Process [" + childProcPid + "] has been killed");
    }
  }

}

export function BluetoothControllerServiceFactory(): BluetoothControllerService {
  return BluetoothControllerService.getInstance();
}

