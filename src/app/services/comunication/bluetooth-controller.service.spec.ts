import { TestBed, inject } from '@angular/core/testing';

import { BluetoothControllerService } from './bluetooth-controller.service';

describe('BluetoothControllerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BluetoothControllerService]
    });
  });

  it('should be created', inject([BluetoothControllerService], (service: BluetoothControllerService) => {
    expect(service).toBeTruthy();
  }));
});
