import {Injectable} from '@angular/core';
import {ChildProcess, fork} from "child_process";

@Injectable()
export class ResourceServerService {
    private childProcess: ChildProcess = null;

    public start(): Promise<any> {
        if (!this.childProcess) {
            return new Promise((resolve, reject) => {
                this.childProcess = fork("resource-server.js", ["10800"]);
                this.childProcess.on('message', (response) => {
                    console.log("Recvd msg from resource server:");
                    console.log(response);
                    resolve(this.childProcess.pid);
                });
                this.childProcess.send({messageType: 'HELLO'});
            });
        } else {
            return null;
        }
    }

    public stop() {
        if (this.childProcess) {
            const childProcPid = this.childProcess.pid;
            this.childProcess.kill();
            this.childProcess = null;
            console.log("Process of resource server [pid " + childProcPid + "] has been killed");
        }
    }
}
