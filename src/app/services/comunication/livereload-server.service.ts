import {EventEmitter, Injectable} from '@angular/core';
import {SERVER_PORT} from "../../../settings";
import {Message} from "../../../Model/Message";
import {Subject, Observable, Observer} from "rxjs";

// import * as WebSocket from "ws";

import * as WebSocketServer from "websocketserver";

const MESSAGES = {
    hello: {
        messageType: 0,
        data: "hello"
    },
    update: {
        messageType: 1,
        data: 0
    },
};

@Injectable()
export class LivereloadServerService {
    protected static instance: LivereloadServerService = null;

    // Return the instance of the service
    public static getInstance(): LivereloadServerService {
        if (LivereloadServerService.instance === null) {
            LivereloadServerService.instance = new LivereloadServerService();
        }
        return LivereloadServerService.instance;
    }

    private clients = [];

    private serverSock: WebSocketServer;

    public messageRecvd: EventEmitter<Message> = new EventEmitter<Message>();

    public clientConnected: EventEmitter<any> = new EventEmitter<any>();
    public clientDisconnected: EventEmitter<any> = new EventEmitter<any>();

    private static getWsRemoteAddr(ws) {
        console.log(ws);
        return 'he'; // ws.upgradeReq.connection.remoteAddress;
    }

    public getClientIpAddresses(): string[] {
        return this.clients.map(c => LivereloadServerService.getWsRemoteAddr(c));
    }

    start() {
        if (this.serverSock) {
            this.stop();
        }
        this.serverSock = new WebSocketServer('all', SERVER_PORT);
        this.serverSock.on('connection', (id) => {
            console.log("Got client ");
            this.clients.push(id);
            this.clientConnected.emit(id);
            this.serverSock.sendMessage('one', JSON.stringify(MESSAGES.hello), id);
        });
        this.serverSock.on('closedconnection', (id) => {
            console.log("Client disconnected");
            this.clients.splice(this.clients.indexOf(id), 1);
            this.clientDisconnected.emit(id);
        });
        this.serverSock.on('message', (data, id) => {
            const mes = this.serverSock.unmaskMessage(data);
            const str = this.serverSock.convertToString(mes.message);
            console.log(str);
            this.serverSock.sendMessage('all', str);
        });
    }

    private createSubj(ws): Subject<Message> {
        const observable = new Observable((obs: Observer<MessageEvent>) => {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);

            return ws.close.bind(ws);
        });

        const observer = {
            next: (data: Object) => {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            },
        };

        return Subject.create(observer, observable)
            .map((response: MessageEvent): Message => {
                const data = JSON.parse(response.data);
                return new Message(data.messageType, data.data);
            });
    }

    stop() {
        if (this.serverSock) {
            this.serverSock.close(err => {
                console.warn("Error while closing server: ");
                console.error(err);
            });
            this.serverSock = null;
            this.clients = [];
        }
    }

    public sendMessage(message: any) {
        this.sendMessageToClients(JSON.stringify(message));
    }

    private sendMessageToClients(message) {
        console.log("Sending message to " + this.clients.length + ' clients:');
        console.log(message);
        this.serverSock.sendMessage('all', message);
    }
}

export const LIVE_RELOAD_SERVER_SERVICE_PROVIDER = {
    provide: LivereloadServerService, useFactory: () => LivereloadServerService.getInstance()
};
