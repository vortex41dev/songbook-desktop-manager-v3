import { TestBed, inject } from '@angular/core/testing';

import { ResourceServerService } from './resource-server.service';

describe('ResourceServerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResourceServerService]
    });
  });

  it('should be created', inject([ResourceServerService], (service: ResourceServerService) => {
    expect(service).toBeTruthy();
  }));
});
