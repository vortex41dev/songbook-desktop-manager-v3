import 'reflect-metadata';
import '../polyfills';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { HomeModule } from './home/home.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HomeComponent} from "./components/home/home.component";
import {SongEditComponent} from "./components/song-edit/song-edit.component";
import {SlideShowComponent} from "./components/slide-show/slide-show.component";
import {ConfirmDialogComponent} from "./components/dialog/confirm-dialog/confirm-dialog.component";
import {SongsListComponent} from "./components/songs-list/songs-list.component";
import {PresenterSidebarComponent} from "./components/presenter-sidebar/presenter-sidebar.component";
import {FileChooserComponent} from "./components/utils/file-chooser/file-chooser.component";
import {PresentationNavigationComponent} from "./components/presentation-navigation/presentation-navigation.component";
import {ServerToolbarIndicatorComponent} from "./components/server-control/server-toolbar-indicator/server-toolbar-indicator.component";
import {BluetoothServerToolbarIndicatorComponent} from "./components/server-control/bluetooth-server-toolbar-indicator/bluetooth-server-toolbar-indicator.component";
import {ImportExportComponent} from "./components/dialog/import-export/import-export.component";
import {FileDbImportExportComponent} from "./components/import-export/file-db-import-export/file-db-import-export.component";
import {PresentationEditorComponent} from "./components/presentation-editor/presentation-editor.component";
import {PresentationsListComponent} from "./components/presentations-list/presentations-list.component";
import {OpenSongSongsImportComponent} from "./components/import-export/open-song-songs-import/open-song-songs-import.component";
import {InfoDialogComponent} from "./components/dialog/info-dialog/info-dialog.component";
import {ResourcesLibraryComponent} from "./components/resources-library/resources-library.component";
import {ResourceServerToolbarIndicatorComponent} from "./components/server-control/resource-server-toolbar-indicator/resource-server-toolbar-indicator.component";
import {ClockComponent} from "./components/clock/clock.component";
import {MailtoLinkDirective} from "./home/directives/mailto-link.directive";
import {ElectronService} from "./providers/electron.service";
import {LocalStorageModule} from "angular-2-local-storage";
import {SortablejsModule} from "ngx-sortablejs";
import {MatSliderModule} from "@angular/material/slider";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {LocalSongsProviderService} from "./services/data-providers/local/local-songs-provider.service";
import {LocalPresentationProviderService} from "./services/data-providers/local/local-presentation-provider.service";
import {PresentationProviderService} from "./services/data-providers/rest/presentation-provider.service";
import {ConfirmDialogServiceService} from "./services/confirm-dialog-service.service";
import {LocalDatabaseService} from "./services/data-providers/local/local-database.service";
import {SongEditControllerTraitService} from "./services/song-edit-controller-trait.service";
import {SlideshowControllerService} from "./services/slideshow-controller.service";
import {PresentationDialogsControllerService} from "./services/presentation-dialogs-controller.service";
import {SongTransponerService} from "./services/data-providers/local/song-transponer.service";
import {ResourceServerService} from "./services/comunication/resource-server.service";
import {RestSongProvider} from "./services/data-providers/rest/rest-song-provider.service";
import {WebSocketService} from "./services/websocket.service";
import {IndexedDbManagerService} from "./services/data-providers/local/indexed-db-manager.service";
import {ServerProcessService} from "./services/server-process.service";
import {
  BluetoothControllerService,
  BluetoothControllerServiceFactory
} from "./services/comunication/bluetooth-controller.service";
import {OpenSongFormatConverterService} from "./services/song-formats-converters/open-song-format-converter.service";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatMenuItem, MatMenuModule} from "@angular/material/menu";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatSelectModule} from "@angular/material/select";
import {MatIconModule} from "@angular/material/icon";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatTabsModule} from "@angular/material/tabs";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatSnackBarModule} from "@angular/material/snack-bar";

import { SlideShowSplitComponent } from './components/slide-show-split/slide-show-split.component';
import { ExternalDisplayComponent } from './components/external-display/external-display.component';
import { ImagesLibraryComponent } from './components/images-library/images-library/images-library.component';
import { ImagesLibraryOpenButtonComponent } from './components/images-library/images-library-open-button/images-library-open-button.component';
import {ImageProvider} from "./services/data-providers/abstract/image-provider";
import {LocalImageProviderService} from "./services/data-providers/local/local-image-provider.service";
import {SlideShowSlideGroupLivePreviewComponent} from "./components/slide-show-slide-group-live-preview/slide-show-slide-group-live-preview.component";
import {QuillModule} from "ngx-quill";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SlideShowComponent,
    SongsListComponent,
    SongEditComponent,
    ConfirmDialogComponent,
    PresentationsListComponent,
    PresenterSidebarComponent,
    PresentationNavigationComponent,
    PresentationEditorComponent,
    FileChooserComponent,
    ImportExportComponent,
    FileDbImportExportComponent,
    ServerToolbarIndicatorComponent,
    BluetoothServerToolbarIndicatorComponent,
    OpenSongSongsImportComponent,
    InfoDialogComponent,
    MailtoLinkDirective,
    ResourcesLibraryComponent,
    ClockComponent,
    ResourceServerToolbarIndicatorComponent,
    SlideShowSplitComponent,
    ExternalDisplayComponent,
    ImagesLibraryComponent,
    ImagesLibraryOpenButtonComponent,
    SlideShowSlideGroupLivePreviewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    HomeModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTooltipModule,
    MatMenuModule,
    MatSidenavModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatIconModule,
    MatToolbarModule,
    MatInputModule,
    MatListModule,
    MatProgressBarModule,
    MatTabsModule,
    MatGridListModule,
    MatSnackBarModule,
    LocalStorageModule.forRoot({
      prefix: 'songbook-app',
      storageType: 'localStorage'
    }),
    SortablejsModule,
    QuillModule.forRoot(),
  ],
  providers: [
    ElectronService,
    LocalSongsProviderService,
    LocalPresentationProviderService,
    LocalDatabaseService,
    SongTransponerService,
    RestSongProvider,
    SongEditControllerTraitService,
    ConfirmDialogServiceService,
    ResourceServerService,
    WebSocketService,
    SlideshowControllerService,
    PresentationProviderService,
    PresentationDialogsControllerService,
    IndexedDbManagerService,
    // LIVE_RELOAD_SERVER_SERVICE_PROVIDER,
    ServerProcessService,
    // BLUETOOTH_SERVER_SERVICE_PROVIDER,
    {
      provide: BluetoothControllerService,
      useFactory: BluetoothControllerServiceFactory
    },
    {provide: ImageProvider, useClass: LocalImageProviderService},
    OpenSongFormatConverterService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    SongEditComponent,
    ConfirmDialogComponent,
    PresentationEditorComponent,
    ImportExportComponent,
    InfoDialogComponent,
    ImagesLibraryComponent,
  ]
})
export class AppModule {}
