export interface SlideShow {

  getCurrentSectionIndex(): number;

  sectionChangePrev();

  sectionChangeNext();

  restartSlideShow();
}
