import {Component, Input} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Song, SongSection} from "../../../Model/Song";
import {SlideShow} from "./SlideShow";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";

const CHANGE_SLIDE_TRANSITION_TIME = 250;

@Component({
  selector: 'app-slide-show',
  templateUrl: './slide-show.component.html',
  styleUrls: ['./slide-show.component.scss'],
  animations: [
    trigger('visibility', [
      state('visible', style({
        opacity: '1'
      })),
      state('hidden', style({
        opacity: '0'
      })),
      transition('hidden <=> visible', animate(CHANGE_SLIDE_TRANSITION_TIME + 'ms ease-in-out')),
    ])
  ]
})
export class SlideShowComponent implements SlideShow {

  private _song;
  @Input()
  set song(song: Song) {
    this._song = song;
    this.currentSectionIndex = 0;
    if (song) {
      this.visibilityState = "hidden";
      setTimeout(() => {
        this.currentSection = song.sections[this.currentSectionIndex];
        this.sectionHtml = this.domSanitizer.bypassSecurityTrustHtml(this.currentSection.text);
        this.visibilityState = "visible";
      }, CHANGE_SLIDE_TRANSITION_TIME);
    }
  };

  get song() {
    return this._song;
  };

  @Input()
  public chordsVisible: boolean = false;

  @Input()
  currentSection: SongSection;

  @Input()
  sectionHtml: SafeHtml | null = null;

  @Input()
  nextSection: number;

  public currentSectionIndex = 0;

  public getCurrentSectionIndex() {
    return this.currentSectionIndex;
  }

  public visibilityState = "hidden";

  constructor(public domSanitizer: DomSanitizer) {
  }

  public sectionChangePrev() {
    if (!this.currentSection) {
      this.sectionHtml = null;
      return;
    }
    if (this.currentSectionIndex > 0) {
      this.visibilityState = "hidden";
      this.currentSectionIndex--;
      setTimeout(() => {
        this.currentSection = this.song.sections[this.currentSectionIndex];
        this.sectionHtml = this.domSanitizer.bypassSecurityTrustHtml(this.currentSection.text);
        this.visibilityState = "visible";
      }, CHANGE_SLIDE_TRANSITION_TIME);
      return true;
    }
    return false;
  }

  public sectionChangeNext() {
    if (!this.currentSection) {
      this.sectionHtml = null;
      return;
    }
    if (this.currentSectionIndex + 1 < this.song.sections.length) {
      this.visibilityState = "hidden";
      this.currentSectionIndex++;
      setTimeout(() => {
        this.currentSection = this.song.sections[this.currentSectionIndex];
        this.sectionHtml = this.domSanitizer.bypassSecurityTrustHtml(this.currentSection.text);
        this.visibilityState = "visible";
      }, CHANGE_SLIDE_TRANSITION_TIME);
      return true;
    }
    return false;
  }

  restartSlideShow() {
    if (!this.currentSection) {
      this.sectionHtml = null;
      return;
    }
    if (this.currentSectionIndex > 0) {
      this.visibilityState = "hidden";
      this.currentSectionIndex = 0;
      setTimeout(() => {
        this.currentSection = this.song.sections[this.currentSectionIndex];
        this.sectionHtml = this.domSanitizer.bypassSecurityTrustHtml(this.currentSection.text);
        this.visibilityState = "visible";
      }, CHANGE_SLIDE_TRANSITION_TIME);
      return true;
    }
    return false;
  }
}
