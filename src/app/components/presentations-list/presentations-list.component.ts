import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Presentation} from "../../../Model/Presentation";
import {LocalPresentationProviderService} from "../../services/data-providers/local/local-presentation-provider.service";

@Component({
  selector: 'app-presentations-list',
  templateUrl: './presentations-list.component.html',
  styleUrls: ['./presentations-list.component.scss']
})
export class PresentationsListComponent implements OnInit {

  @Input()
  presentations: Presentation[] = [];

  @Output()
  setPresentation: EventEmitter<Presentation> = new EventEmitter<Presentation>();

  constructor(private presentationsProvider: LocalPresentationProviderService) {
    this.loadPresentations();
  }

  private loadPresentations() {
    this.presentationsProvider.getPresentations().then(presentations => {
      this.presentations = presentations.sort((l, r) => l.name < r.name ? -1 : l.name == r.name ? 0 : 1);
    });
  }

  ngOnInit() {
    this.presentationsProvider.listChanged.subscribe(x => {
      this.loadPresentations();
    });
  }

}
