import {Component} from '@angular/core';
import {interval} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent {

  public clock = interval(1000)
    .pipe(
      map(() => new Date())
    )
}
