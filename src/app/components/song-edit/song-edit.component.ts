import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {createSongSection, Song, SongSection} from "../../../Model/Song";
import {RestSongProvider} from "../../services/data-providers/rest/rest-song-provider.service";
import {LocalSongsProviderService} from "../../services/data-providers/local/local-songs-provider.service";

@Component({
    selector: 'app-song-edit',
    templateUrl: './song-edit.component.html',
    styleUrls: ['./song-edit.component.scss']
})
export class SongEditComponent implements OnInit {

    @Input()
    song: Song;

    @Output()
    songCreated = new EventEmitter();

    @Output()
    songChanged = new EventEmitter();

    @Input()
    savingInProgress: boolean = false;

    @Input()
    message: string = '';

    constructor(private songProvider: LocalSongsProviderService,
                public dialogRef: MatDialogRef<SongEditComponent>) {
    }

    ngOnInit() {
    }

    save() {
        this.savingInProgress = true;
        if (this.song.id === '0') {
            this.songProvider
                .saveSongAsNew(this.song)
                .then(newSong => {
                    this.songCreated.emit(newSong);
                    this.savingInProgress = false;
                    this.message = 'Zapisano';
                    setTimeout(() => {
                        this.message = '';
                    }, 2000);
                });
        } else {
            this.songProvider
                .saveSong(this.song)
                .then(song => {
                    this.songChanged.emit(song);
                    this.savingInProgress = false;
                    this.message = 'Zapisano';
                    setTimeout(() => {
                        this.message = '';
                    }, 2000);
                });
        }
    }

    addNewSection() {
        this.song.sections.push(createSongSection());
    }

    removeSection(section: SongSection) {
        this.song.sections.splice(this.song.sections.indexOf(section), 1);
    }

    duplicateSection(section: SongSection) {
        this.song.sections.splice(this.song.sections.indexOf(section), 0, createSongSection(section));
    }

    trimSection(section: SongSection) {
        console.log(section);
        section.text = SongEditComponent.trimTextBlock(section.text);
        section.chords = SongEditComponent.trimTextBlock(section.chords);
    }

    private static trimTextBlock(textBlock): string {
        if (textBlock) {
            return textBlock
                .split('\n')
                .map(line => line.trim())
                .filter(l => !!l)
                .join('\n')
                .replace(/^[\n\r]+/, '')
                .replace(/[\n\r]+$/, '');
        }
        return textBlock;
    }

    close() {
        this.dialogRef.close();
    }

}
