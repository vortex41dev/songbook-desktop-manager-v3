import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideShowSplitComponent } from './slide-show-split.component';

describe('SlideShowSplitComponent', () => {
  let component: SlideShowSplitComponent;
  let fixture: ComponentFixture<SlideShowSplitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideShowSplitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideShowSplitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
