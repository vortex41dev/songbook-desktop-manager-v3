import {Component, Input, OnInit} from '@angular/core';
import {Song, SongSection} from "../../../Model/Song";
import {SlideShow} from "../slide-show/SlideShow";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";

const CHANGE_SLIDE_TRANSITION_TIME = 250;

@Component({
  selector: 'app-slide-show-split',
  templateUrl: './slide-show-split.component.html',
  styleUrls: ['./slide-show-split.component.scss']
})
export class SlideShowSplitComponent implements OnInit, SlideShow {

  @Input()
  public chordsVisible: boolean = false;

  public currentSectionIndex: number = 0;

  public topSecIndex: number;

  public bottomSecIndex: number;

  public getCurrentSectionIndex() {
    return this.currentSectionIndex;
  }

  private visibilityState = "hidden";

  @Input()
  topSection: SongSection | null;

  @Input()
  topSectionHtml: SafeHtml | null = null;

  @Input()
  bottomSection: SongSection | null;

  @Input()
  bottomSectionHtml: SafeHtml | null = null;

  private _song: Song;

  @Input()
  set song(song) {
    this._song = song;
    this.currentSectionIndex = 0;
    if (song) {
      this.visibilityState = "hidden";
      setTimeout(() => {
        this.updateCurrentSections();
        this.visibilityState = "visible";
      }, CHANGE_SLIDE_TRANSITION_TIME);
    }
  };

  get song() {
    return this._song;
  };

  constructor(public domSanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  restartSlideShow() {
    this.currentSectionIndex = 0;
    this.updateCurrentSections();
  }

  sectionChangeNext() {
    if (this.currentSectionIndex < this._song.sections.length - 1) {
      this.currentSectionIndex++;
      this.updateCurrentSections();
      return true;
    }
    return false;
  }

  sectionChangePrev() {
    if (this.currentSectionIndex >= 1) {
      this.currentSectionIndex--;
      this.updateCurrentSections();
      return true;
    }
    return false;
  }

  private updateCurrentSections() {
    const sections = this._song.sections;
    if (!sections.length) {
      return;
    }
    this.topSecIndex = (1 + Math.floor((this.currentSectionIndex - 1) / 2)) * 2;
    this.bottomSecIndex = (Math.floor(this.currentSectionIndex / 2)) * 2 + 1;

    this.topSection = this.topSecIndex < sections.length ? sections[this.topSecIndex] : sections[0];
    this.bottomSection = this.bottomSecIndex < sections.length ? sections[this.bottomSecIndex] : sections[0];
    if (this.topSection === this.bottomSection) {
      this.bottomSection = null;
    }
    this.topSectionHtml = this.topSection ? this.domSanitizer.bypassSecurityTrustHtml(this.topSection.text) : null;
    this.bottomSectionHtml = this.bottomSection ? this.domSanitizer.bypassSecurityTrustHtml(this.bottomSection.text) : null;
  }
}
