import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageSlideGroup} from "../../../../Model/SlideGroup";
import {ImageProvider} from "../../../services/data-providers/abstract/image-provider";
import {FileListConverter} from "../../../utils/file-list-converter";
import {BinaryFileReader} from "../../../utils/text-file-reader";
import {UUID} from "angular2-uuid";
import {ResourceFileSystemService} from "../../../services/resource-file-system/resource-file-system.service";

@Component({
  selector: 'app-images-library',
  templateUrl: './images-library.component.html',
  styleUrls: ['./images-library.component.scss']
})
export class ImagesLibraryComponent implements OnInit {

  @Input()
  images: ImageSlideGroup[] = [];

  @Output()
  addImage: EventEmitter<ImageSlideGroup> = new EventEmitter();

  constructor(public imagesProvider: ImageProvider, public resourceFileSystemService: ResourceFileSystemService) {

  }

  ngOnInit() {
    this.imagesProvider.getAllImages().then(images => this.images = images);
  }

  imageSelected(image: ImageSlideGroup) {
    this.addImage.emit(image);
  }

  createNewImages(files: FileList) {
    const filesArr: File[] = FileListConverter.toArray(files);
    if (filesArr && filesArr.length > 0) {
      return filesArr.map(file => this.imagesProvider.saveImage(UUID.UUID(), file.name, file));
    }
    return Promise.resolve();
  }
}
