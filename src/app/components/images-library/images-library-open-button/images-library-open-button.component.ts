import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ImagesLibraryComponent} from "../images-library/images-library.component";

@Component({
  selector: 'app-images-library-open-button',
  templateUrl: './images-library-open-button.component.html',
  styleUrls: ['./images-library-open-button.component.scss']
})
export class ImagesLibraryOpenButtonComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openImagesLibrary() {
    const dlg = this.dialog.open(ImagesLibraryComponent);
    dlg.afterClosed().subscribe(x => {

    });
  }

}
