import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagesLibraryOpenButtonComponent } from './images-library-open-button.component';

describe('ImagesLibraryOpenButtonComponent', () => {
  let component: ImagesLibraryOpenButtonComponent;
  let fixture: ComponentFixture<ImagesLibraryOpenButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagesLibraryOpenButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagesLibraryOpenButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
