import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

    @Input()
    question: string;

    @Output()
    onOk = new EventEmitter();

    @Output()
    onCancel = new EventEmitter();

    okClicked() {
        this.onOk.emit();
    }

    cancelClicked() {
        this.onCancel.emit();
    }

}
