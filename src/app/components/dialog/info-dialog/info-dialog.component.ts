import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialogRef} from "@angular/material";

@Component({
    selector: 'app-info-dialog',
    templateUrl: './info-dialog.component.html',
    styleUrls: ['./info-dialog.component.scss']
})
export class InfoDialogComponent implements OnInit {

    @Output()
    onOk: EventEmitter<any> = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<InfoDialogComponent>) {
    }

    ngOnInit() {
    }

    okClicked() {
        this.dialogRef.close();
        this.onOk.emit();
    }

}
