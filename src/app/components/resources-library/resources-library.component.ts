import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-resources-library',
    templateUrl: './resources-library.component.html',
    styleUrls: ['./resources-library.component.scss']
})
export class ResourcesLibraryComponent implements OnInit {

    images = [];

    constructor() {
    }

    ngOnInit() {
    }

}
