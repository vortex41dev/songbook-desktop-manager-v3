import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Presentation} from "../../../Model/Presentation";
import {DUR_TONATION, MOL_TONATION} from "../../app.component";
import {MatDialogRef} from "@angular/material";
import {Song} from "../../../Model/Song";
import {createSongSlideGroup, ImageSlideGroup, SlideGroup, SongSlideGroup} from "../../../Model/SlideGroup";
import {LocalPresentationProviderService} from "../../services/data-providers/local/local-presentation-provider.service";

@Component({
  selector: 'app-presentation-editor',
  templateUrl: './presentation-editor.component.html',
  styleUrls: ['./presentation-editor.component.scss']
})
export class PresentationEditorComponent implements OnInit {

  @Input()
  presentation: Presentation;

  @Input()
  savingInProgress: boolean = false;

  @Input()
  message: string = '';

  @Output()
  presentationChanged = new EventEmitter();

  @Output()
  presentationCreated = new EventEmitter();

  durTonation = DUR_TONATION;
  molTonation = MOL_TONATION;


  constructor(private presentationProvider: LocalPresentationProviderService,
              public dialogRef: MatDialogRef<PresentationEditorComponent>) {
  }

  ngOnInit() {

  }

  addSong(song: Song) {
    this.presentation.slideGroups.push(createSongSlideGroup(song));
    return this;
  }

  addImage(image: ImageSlideGroup) {
    this.presentation.slideGroups.push(image.clone());
    return this;
  }

  removeSlidesGroup(slideGroup: SlideGroup) {
    this.presentation.slideGroups.splice(this.presentation.slideGroups.indexOf(slideGroup), 1);
  }

  save() {
    this.savingInProgress = true;
    if (this.presentation.id === 0) {
      this.presentationProvider
        .saveNewPresentation(this.presentation)
        .then(presentation => {
          this.presentationCreated.emit(presentation);
          this.savingInProgress = false;
          this.message = 'Prezentacja została zapisana';
          setTimeout(() => {
            this.message = '';
          }, 2000);
        });
    } else {
      this.presentationProvider
        .savePresentation(this.presentation)
        .then(presentation => {
          this.presentationChanged.emit(presentation);
          this.savingInProgress = false;
          this.message = 'Prezentacja została zapisana';
          setTimeout(() => {
            this.message = '';
          }, 2000);
        });
    }
  }

  close() {
    this.dialogRef.close();
  }

}
