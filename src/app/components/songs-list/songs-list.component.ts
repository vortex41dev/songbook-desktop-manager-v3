import {Component, OnInit, Output, EventEmitter, Input, ViewChild} from '@angular/core';
import {Song} from "../../../Model/Song";
import {LocalSongsProviderService} from "../../services/data-providers/local/local-songs-provider.service";

@Component({
    selector: 'app-songs-list',
    templateUrl: './songs-list.component.html',
    styleUrls: ['./songs-list.component.scss']
})
export class SongsListComponent implements OnInit {

    @Output()
    currentSongChange = new EventEmitter();

    @Output()
    addSong: EventEmitter<Song> = new EventEmitter<Song>();

    @Input()
    enableAddMode: boolean = false;

    _currentSong: Song = null;
    @Input()
    get currentSong(): Song {
        return this._currentSong;
    }

    @ViewChild('songsList', {static: true})
    private songsListView;

    @ViewChild('searchInput', {static: true})
    private searchInput;


    private __emitSongChangedGuard = false;

    set currentSong(song: Song) {
        this._currentSong = song;
        if (this.__emitSongChangedGuard) {
            this.currentSongChange.emit(this._currentSong);
        }
    }

    @Input()
    preSelected: Song = null;

    @Input()
    preSelectedIndex: number = null;

    @Input()
    songs: Song[] = [];


    @Input()
    filterPhrase: string = '';

    setCurrentSong(song) {
        this.__emitSongChangedGuard = true;
        this.currentSong = song;
        this.__emitSongChangedGuard = false;
    }

    constructor(private songsProvider: LocalSongsProviderService) {
    }

    ngOnInit() {
        this.songsProvider.getSongsList()
            .then(songs => this.songs = songs);
        this.songsProvider.listChanged.subscribe(x => {
            this.songsProvider.getSongsList()
                .then(songs => {
                    this.songs = songs;
                });
        });
    }

    songFilterKeypress($event) {
        if ($event.code === 'Backquote') {
            $event.preventDefault();
        }
        switch ($event.code) {
            case 'ArrowUp':
                this.setPreviousPreSelect();
                break;
            case 'ArrowDown':
                this.setNextPreSelect();
                break;
            case 'Enter':
                if (this.preSelected) {
                    this.setCurrentSong(this.preSelected);
                    this.preSelected = null;
                    this.preSelectedIndex = null;
                }
                break;
        }
    }

    songFilterKeyUp($event) {
        switch ($event.code) {
            case 'ArrowUp':
                this.setPreviousPreSelect();
                break;
            case 'ArrowDown':
                this.setNextPreSelect();
                break;
            default:
                this.updatePreSelect();
        }
    }

    updatePreSelect() {
        this.preSelectedIndex = null;
        this.preSelected = null;
        this.setNextPreSelect();
    }

    private setPreviousPreSelect() {
        if (this.preSelectedIndex === null) {
            return;
        }
        let i = this.preSelectedIndex - 1;
        // find prev visible
        while (i >= 0 && this.filterPhrase && SongsListComponent.checkSearchSong(this.songs[i], this.filterPhrase)) {
            i--;
        }
        if (i >= 0) {
            this.preSelectedIndex = i;
            this.preSelected = this.songs[i];
        }
    }

    private setNextPreSelect() {
        let i = this.preSelectedIndex === null ? 0 : this.preSelectedIndex + 1;
        // find next visible
        while (i >= 0 && i < this.songs.length && this.filterPhrase
        && SongsListComponent.checkSearchSong(this.songs[i], this.filterPhrase)) {
            i++;
        }
        if (i >= 0 && i < this.songs.length) {
            this.preSelectedIndex = i;
            this.preSelected = this.songs[i];
        }
    }

    private static checkSearchSong(song: Song, phrase: string): boolean {
        return song.title.toLowerCase().indexOf(phrase.toLowerCase()) === -1 && song.order.toString().indexOf(phrase.toLowerCase()) === -1;
    }
}
