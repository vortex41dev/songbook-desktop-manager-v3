import {Component, OnInit} from '@angular/core';
import {TextFileReader} from "../../../utils/text-file-reader";
import {Song} from "../../../../Model/Song";
import {OpenSongFormatConverterService} from "../../../services/song-formats-converters/open-song-format-converter.service";
import {LocalSongsProviderService} from "../../../services/data-providers/local/local-songs-provider.service";
import {FileListConverter} from "../../../utils/file-list-converter";

@Component({
    selector: 'app-open-song-songs-import',
    templateUrl: './open-song-songs-import.component.html',
    styleUrls: ['./open-song-songs-import.component.scss']
})
export class OpenSongSongsImportComponent implements OnInit {

    constructor(private converter: OpenSongFormatConverterService,
                private songProvider: LocalSongsProviderService,) {
    }

    ngOnInit() {
    }

    filesChoosen(files) {
        const filesArr: File[] = FileListConverter.toArray(files);
        if (filesArr && filesArr.length > 0) {
            filesArr.map(file => TextFileReader.readTextFile(file)
                .then((openSongXml: string) => {
                    this.converter.convertFromFileData(openSongXml)
                        .then((song: Song) => {
                            console.log("Przekonwertowany utwór:");
                            console.log(song);
                            this.songProvider.saveSongAsNew(song);
                        });
                }));
        }
    }

}
