import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenSongSongsImportComponent } from './open-song-songs-import.component';

describe('OpenSongSongsImportComponent', () => {
  let component: OpenSongSongsImportComponent;
  let fixture: ComponentFixture<OpenSongSongsImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenSongSongsImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenSongSongsImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
