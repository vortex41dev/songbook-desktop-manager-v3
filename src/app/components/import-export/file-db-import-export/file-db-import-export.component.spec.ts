import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileDbImportExportComponent } from './file-db-import-export.component';

describe('FileDbImportExportComponent', () => {
  let component: FileDbImportExportComponent;
  let fixture: ComponentFixture<FileDbImportExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileDbImportExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileDbImportExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
