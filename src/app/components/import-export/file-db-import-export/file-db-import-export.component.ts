import {Component, Input, OnInit} from '@angular/core';
import {IndexedDbManagerService} from "../../../services/data-providers/local/indexed-db-manager.service";
import {MatSnackBar} from "@angular/material";
import {LocalSongsProviderService} from "../../../services/data-providers/local/local-songs-provider.service";
import {LocalPresentationProviderService} from "../../../services/data-providers/local/local-presentation-provider.service";
import {ConfirmDialogServiceService} from "../../../services/confirm-dialog-service.service";
import {UUID} from "angular2-uuid";

@Component({
    selector: 'app-file-db-import-export',
    templateUrl: './file-db-import-export.component.html',
    styleUrls: ['./file-db-import-export.component.scss']
})
export class FileDbImportExportComponent implements OnInit {

    constructor(private dbManagerService: IndexedDbManagerService,
                private snackBar: MatSnackBar,
                private songsProvider: LocalSongsProviderService,
                private presentationsProvider: LocalPresentationProviderService,
                private confirmDlgService: ConfirmDialogServiceService,) {
    }

    @Input()
    isWorking: boolean = false;

    public exportDbToFile() {
        this.dbManagerService.exportStorage();
    }

    public fileChosen(files) {
        console.log("Mam plik:");
        console.log(files);
        if (files && files.length > 0) {
            const importLambda = (clearDb) => {
                this.isWorking = true;
                this.importDataFromFile(files[0], clearDb).then(() => {
                    this.isWorking = false;
                    Promise.all([
                        this.songsProvider.getSongsList(),
                        this.presentationsProvider.getPresentations()
                    ]).then(elements => {
                        const [songs, presentations] = elements;
                        const msg = "Import zakończony. Zaimportowano " + songs.length + ' oraz '
                            + presentations.length + ' prezentacji';
                        this.snackBar.open(msg, 'ok', {
                            duration: 3000
                        });

                        this.presentationsProvider.listChanged.emit(true);
                        this.songsProvider.listChanged.emit(true);
                    });
                });
            };
            this.confirmDlgService.createConfirmDialog("Czy wyczyścić bazę danych?",
                () => importLambda(true),
                () => importLambda(false));
        }
    }

    private importDataFromFile(file: Blob, clearDb: boolean = false) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            // If we use onloadend, we need to check the readyState.
            reader.onloadend = (evt) => {
                const target: any = <any>evt.target;
                if (target.readyState === 2) { // done
                    let storeRestoredFromFile = JSON.parse(target.result);
                    if (FileDbImportExportComponent.exportedDataContainsLegacyIds(storeRestoredFromFile)) {
                        storeRestoredFromFile = this.changeDataLegacyIdsToUuids(storeRestoredFromFile);
                    }
                    this.dbManagerService.importStorage(storeRestoredFromFile, clearDb).then(resolve).catch(reject);
                } else {
                    reject();
                }
            };
            reader.readAsText(file);
        });
    }

    private static exportedDataContainsLegacyIds(data: any): boolean {
        const uuidLength = UUID.UUID();
        for (const song of data[LocalSongsProviderService.getIndexName()]) {
            if (song.id.toString().length !== uuidLength) {
                return true;
            }
        }
        for (const presentation of data[LocalPresentationProviderService.getIndexName()]) {
            if (presentation.id.toString().length !== uuidLength) {
                return true;
            }
        }
        return false;
    }

    private changeDataLegacyIdsToUuids(data) {
        const getSongIdDictionaryKeyForId = (id) => '_' + id;
        const uuidLength = UUID.UUID();
        const isUuid = (sth) => sth.toString().length === uuidLength;
        const songIdDictionary = {};
        for (const song of data[LocalSongsProviderService.getIndexName()]) {
            if (!isUuid(song.id)) {
                const uuid = UUID.UUID();
                songIdDictionary[getSongIdDictionaryKeyForId(song.id)] = uuid;
                song.id = uuid;
            }
        }
        for (const presentation of data[LocalPresentationProviderService.getIndexName()]) {
            if (!isUuid(presentation.id)) {
                presentation.id = UUID.UUID();
            }
            for (const songRepr of presentation.slideGroups) {
                if (!isUuid(songRepr.songId)) {
                    songRepr.songId = songIdDictionary[getSongIdDictionaryKeyForId(songRepr.songId)];
                }
            }
        }
        return data;
    }

    ngOnInit() {
    }

}
