import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Song} from "../../../Model/Song";
import {
  ImageSlideGroup,
  SLIDE_GROUP_TYPE_IMAGE,
  SLIDE_GROUP_TYPE_SONG,
  SlideGroup,
  SongSlideGroup
} from "../../../Model/SlideGroup";
import {LocalSongsProviderService} from "../../services/data-providers/local/local-songs-provider.service";
import {SlideShowSplitComponent} from "../slide-show-split/slide-show-split.component";
import {ResourceFileSystemService} from "../../services/resource-file-system/resource-file-system.service";

@Component({
  selector: 'app-slide-show-slide-group-live-preview',
  templateUrl: './slide-show-slide-group-live-preview.component.html',
  styleUrls: ['./slide-show-slide-group-live-preview.component.scss']
})
export class SlideShowSlideGroupLivePreviewComponent implements OnInit {

  @ViewChild('slideShow', {static: false})
  public slideShowComponent: SlideShowSplitComponent;

  @ViewChild('slideShowSplit', {static: false})
  public splitSlideShowComponent: SlideShowSplitComponent;

  @Input()
  public chordsVisible: boolean = false;

  public _slideGroup: SlideGroup;
  @Input()
  public set slideGroup(slideGroup: SlideGroup) {
    this._slideGroup = slideGroup;
    this.updateSlideGroup();
  }

  public get slideGroup(): SlideGroup | null {
    return this._slideGroup;
  }

  @Input()
  public currentSong: Song | null;

  @Input()
  public currentImage: ImageSlideGroup | null;

  @Input()
  public splitMode: boolean = true;

  constructor(private songsProviderService: LocalSongsProviderService, public fileSystem: ResourceFileSystemService) {
  }

  ngOnInit() {
    this.updateSlideGroup();
  }

  private updateSlideGroup() {
    this.clearLocalState();
    if (!this._slideGroup) {
      return null;
    }
    switch (this._slideGroup.type) {
      case SLIDE_GROUP_TYPE_SONG:
        return this.loadSong(this._slideGroup as SongSlideGroup);
      case SLIDE_GROUP_TYPE_IMAGE:
        return this.loadImage(this._slideGroup as ImageSlideGroup);
    }
  }

  private loadSong(songsSlideGroup: SongSlideGroup) {
    return this.songsProviderService.getSong(songsSlideGroup.songId, songsSlideGroup.keyTone)
      .then(s => {
        console.log('load song', s);
        this.currentSong = s;
        return s;
      });
  }

  private clearLocalState() {
    this.currentSong = null;
    this.currentImage = null;
  }

  private loadImage(imageSlideGroup: ImageSlideGroup) {
    this.currentImage = imageSlideGroup;
    console.log(this.currentImage);
    return Promise.resolve(imageSlideGroup);
  }

  restartSlideShow() {
    if (this.getSlideShowComponent()) {
      this.getSlideShowComponent().restartSlideShow();
    }
  }

  sectionChangeNext() {
    console.log(this.getSlideShowComponent());
    if (this.getSlideShowComponent()) {
      return this.getSlideShowComponent().sectionChangeNext();
    }
    return false;
  }

  sectionChangePrev() {
    if (this.getSlideShowComponent()) {
      return this.getSlideShowComponent().sectionChangePrev();
    }
    return false;
  }

  getCurrentSectionIndex() {
    if (this.getSlideShowComponent()) {
      return this.getSlideShowComponent().getCurrentSectionIndex();
    }
    return 0;
  }

  private getSlideShowComponent() {
    return this.splitMode ? this.splitSlideShowComponent : this.slideShowComponent;
  }
}
