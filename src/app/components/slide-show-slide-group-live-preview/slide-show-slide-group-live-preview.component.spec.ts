import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideShowSlideGroupLivePreviewComponent } from './slide-show-slide-group-live-preview.component';

describe('SlideShowSlideGroupLivePreviewComponent', () => {
  let component: SlideShowSlideGroupLivePreviewComponent;
  let fixture: ComponentFixture<SlideShowSlideGroupLivePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideShowSlideGroupLivePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideShowSlideGroupLivePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
