import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatSnackBar} from "@angular/material";
import {PresentationsListComponent} from "../presentations-list/presentations-list.component";
import {PresentationNavigationComponent} from "../presentation-navigation/presentation-navigation.component";
import {Presentation} from "../../../Model/Presentation";
import {AppComponent} from "../../app.component";
import {PresentationDialogsControllerService} from "../../services/presentation-dialogs-controller.service";
import {LocalSongsProviderService} from "../../services/data-providers/local/local-songs-provider.service";
import {SlideGroup} from "../../../Model/SlideGroup";

export const PRESENTER_STATE_PRES_LIST = "pres_list";
export const PRESENTER_STATE_PRES_NAV = "pres_nav";

@Component({
  selector: 'app-presenter-sidebar',
  templateUrl: './presenter-sidebar.component.html',
  styleUrls: ['./presenter-sidebar.component.scss']
})
export class PresenterSidebarComponent implements OnInit {

  @Input()
  state: string = PRESENTER_STATE_PRES_LIST;

  @Input()
  currentPresentation: Presentation = null;

  @Output()
  currentSlideGroupChange: EventEmitter<SlideGroup> = new EventEmitter<SlideGroup>();

  @ViewChild('presentationList', {static: false})
  presentationsListComponent: PresentationsListComponent;

  @ViewChild('presentationPreview', {static: false})
  public presentationPreview: PresentationNavigationComponent;

  changeSlideGroup(sg: SlideGroup) {
    this.currentSlideGroupChange.emit(sg);
  }

  @Input()
  parentController: AppComponent;

  constructor(private songsProvider: LocalSongsProviderService,
              private snackBar: MatSnackBar,
              private presentationDialogsController: PresentationDialogsControllerService) {
  }

  ngOnInit() {
  }

  setCurrentPresentation(presentation: Presentation) {
    this.currentPresentation = presentation;
    this.setPresNavMode();
  }

  setPresListMode() {
    this.state = PRESENTER_STATE_PRES_LIST;
  }

  setPresNavMode() {
    this.state = PRESENTER_STATE_PRES_NAV;
  }

  newPresentation() {
    this.parentController.keyboardNavigationEnabled = false;
    this.presentationDialogsController.newPresentationDialog(presentation => {
      this.snackBar.open("Prezentacja \"" + presentation.name + "\" została utworzona", 'ok', {
        duration: 3000
      });
      this.parentController.keyboardNavigationEnabled = true;
      if (this.presentationsListComponent) {
        this.presentationsListComponent.presentations.unshift(presentation);
      }
    });
  }


  editPresentation(presentation: Presentation) {
    this.parentController.keyboardNavigationEnabled = false;
    this.presentationDialogsController.editPresentationDialog(presentation, pres => {
      this.parentController.keyboardNavigationEnabled = true;
      this.snackBar.open("Prezentacja \"" + pres.name + "\" została utworzona", 'ok', {
        duration: 3000
      });
    });
  }


  deletePresentation(presentation: Presentation) {
    this.parentController.keyboardNavigationEnabled = false;
    this.presentationDialogsController.deletePresentationDialog(presentation, p => {
      this.parentController.keyboardNavigationEnabled = true;
      this.snackBar.open("Prezentacja \"" + presentation.name + "\" została usunięta", 'ok', {
        duration: 3000
      });
    });
  }

}
