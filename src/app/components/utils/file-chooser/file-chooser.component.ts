import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-file-chooser',
    templateUrl: './file-chooser.component.html',
    styleUrls: ['./file-chooser.component.scss']
})
export class FileChooserComponent implements OnInit {

    constructor() {
    }

    @Input()
    acceptedMimeTypes: string = '*/*';

    @Input()
    multiple: boolean = false;

    @Output()
    fileChosen: EventEmitter<FileList> = new EventEmitter<FileList>();

    fileChanged($event) {
        console.log($event);
        const files = $event.target.files;
        console.log(files);
        if (files && files.length > 0) {
            this.fileChosen.emit(files);
        }
    }

    ngOnInit() {
    }

}
