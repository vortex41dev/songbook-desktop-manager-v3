import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-external-display-button',
  templateUrl: './external-display.component.html',
  styleUrls: ['./external-display.component.scss']
})
export class ExternalDisplayComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  openExternalWindow() {
    let url = 'http://localhost:10800/index.html';
    console.log(url);
    window.open(url, '_blank', 'nodeIntegration=no');
  }
}
