import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalDisplayComponent } from './external-display.component';

describe('ExternalDisplayComponent', () => {
  let component: ExternalDisplayComponent;
  let fixture: ComponentFixture<ExternalDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
