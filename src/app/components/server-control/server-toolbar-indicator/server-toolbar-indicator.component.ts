import {Component, Input, OnInit} from '@angular/core';
import {ServerProcessService} from "../../../services/server-process.service";
import {WebSocketService} from "../../../services/websocket.service";
import {SERVER_ADDRESS} from "../../../../settings";

@Component({
  selector: 'app-server-toolbar-indicator',
  templateUrl: './server-toolbar-indicator.component.html',
  styleUrls: ['./server-toolbar-indicator.component.scss']
})
export class ServerToolbarIndicatorComponent implements OnInit {

  private _serverIsRunning: boolean = false;
  @Input()
  get serverIsRunning(): boolean {
    return this._serverIsRunning;
  }

  set serverIsRunning(value: boolean) {
    this._serverIsRunning = value;
    if (value) {
      const p = this.server.start();
      if (p) {
        this.wsService.disconnect();
        p.then(() => {
          this.wsService.connect(SERVER_ADDRESS);
        }).catch((reason) => {
          console.log('ERROR while child process:', reason);
          this.wsService.disconnect();
        });
      }
    } else {
      this.server.stop();
      this.wsService.disconnect();
    }
  }

  @Input()
  serverClients: string[] = [];

  constructor(private server: ServerProcessService,
              private wsService: WebSocketService) {
  }

  ngOnInit() {
    this.server.clientListChanged.subscribe((clients) => {
      console.log(clients);
      this.serverClients = clients;
    });
  }

}
