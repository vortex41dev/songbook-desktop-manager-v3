import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerToolbarIndicatorComponent } from './server-toolbar-indicator.component';

describe('ServerToolbarIndicatorComponent', () => {
  let component: ServerToolbarIndicatorComponent;
  let fixture: ComponentFixture<ServerToolbarIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerToolbarIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerToolbarIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
