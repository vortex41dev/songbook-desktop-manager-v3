import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BluetoothServerToolbarIndicatorComponent } from './bluetooth-server-toolbar-indicator.component';

describe('BluetoothServerToolbarIndicatorComponent', () => {
  let component: BluetoothServerToolbarIndicatorComponent;
  let fixture: ComponentFixture<BluetoothServerToolbarIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluetoothServerToolbarIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BluetoothServerToolbarIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
