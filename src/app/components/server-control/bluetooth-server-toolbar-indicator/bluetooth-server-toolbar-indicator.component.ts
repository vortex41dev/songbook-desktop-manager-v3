import {Component, Input, NgZone, OnInit} from '@angular/core';
import {BluetoothControllerService} from "../../../services/comunication/bluetooth-controller.service";
import {BLUETOOTH_CONTROLLER_SETTINGS} from "../../../../settings";

@Component({
    selector: 'app-bluetooth-server-toolbar-indicator',
    templateUrl: './bluetooth-server-toolbar-indicator.component.html',
    styleUrls: ['./bluetooth-server-toolbar-indicator.component.scss']
})
export class BluetoothServerToolbarIndicatorComponent implements OnInit {

    private _serverIsRunning: boolean = false;
    @Input()
    get serverIsRunning(): boolean {
        return this._serverIsRunning;
    }

    set serverIsRunning(value: boolean) {
        this._serverIsRunning = value;
        console.log(value);
        if (value) {
            const p = this.server.start(
                BLUETOOTH_CONTROLLER_SETTINGS.btDestAddress.toString(),
                BLUETOOTH_CONTROLLER_SETTINGS.btDestChannel.toString()
            );
        } else {
            this.server.stop();
        }
    }

    serverStatesPl = {
        disconnected: 'rozłączony',
        connecting: 'łączenie',
        connected: 'połączono',
        error: 'błąd',
    };

    toggleServerIsRunning() {
        this.serverIsRunning = !this.serverIsRunning;
    }

    @Input()
    serverState: string = 'disconnected';

    constructor(public server: BluetoothControllerService,
                private _ngzone: NgZone,) {

    }

    ngOnInit() {
        this.server.connectionStateChanged.subscribe(stateObj => {
            this._ngzone.run(() => {
                this.serverState = stateObj.state;
            });
        });
    }
}
