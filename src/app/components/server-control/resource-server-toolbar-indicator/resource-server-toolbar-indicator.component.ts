import {Component, Input, OnInit} from '@angular/core';
import {ResourceServerService} from "../../../services/comunication/resource-server.service";

@Component({
    selector: 'app-resource-server-toolbar-indicator',
    templateUrl: './resource-server-toolbar-indicator.component.html',
    styleUrls: ['./resource-server-toolbar-indicator.component.scss']
})
export class ResourceServerToolbarIndicatorComponent implements OnInit {

    constructor(private server: ResourceServerService,) {
    }

    ngOnInit() {
    }

    private _serverIsRunning: boolean = false;
    @Input()
    get serverIsRunning(): boolean {
        return this._serverIsRunning;
    }

    set serverIsRunning(value: boolean) {
        this._serverIsRunning = value;
        if (value) {
            this.server.start()
                .then(pid => {
                    console.log("Resource server started. Pid: " + pid);
                })
                .catch(reason => {
                    console.log("Failed to start resource server." + reason);
                })
        } else {
            this.server.stop();
        }
    }

}
