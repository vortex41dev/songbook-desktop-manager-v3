import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceServerToolbarIndicatorComponent } from './resource-server-toolbar-indicator.component';

describe('ResourceServerToolbarIndicatorComponent', () => {
  let component: ResourceServerToolbarIndicatorComponent;
  let fixture: ComponentFixture<ResourceServerToolbarIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceServerToolbarIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceServerToolbarIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
