import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Presentation} from "../../../Model/Presentation";
import {
  ImageSlideGroup,
  SlideGroup, SongSlideGroup,
} from "../../../Model/SlideGroup";
import {SongEditControllerTraitService} from "../../services/song-edit-controller-trait.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LocalSongsProviderService} from "../../services/data-providers/local/local-songs-provider.service";
import {ResourceFileSystemService} from "../../services/resource-file-system/resource-file-system.service";

@Component({
  selector: 'app-presentation-navigation',
  templateUrl: './presentation-navigation.component.html',
  styleUrls: ['./presentation-navigation.component.scss']
})
export class PresentationNavigationComponent implements OnInit {

  @Input()
  presentation: Presentation;

  @Input()
  currentSlideGroupId: any = 0;

  @Output()
  setSlideGroup: EventEmitter<SlideGroup> = new EventEmitter<SlideGroup>();

  constructor(private songEditController: SongEditControllerTraitService,
              private snackBar: MatSnackBar,
              private songProvier: LocalSongsProviderService,
              private resourceFileSystemService: ResourceFileSystemService,
  ) {
  }

  ngOnInit() {
  }

  setCurrentSongSlideGroup(sg: SlideGroup) {
    this.setSlideGroup.emit(sg);
    this.currentSlideGroupId = sg.getId();
  }

  nextSong() {
    if (this.presentation.slideGroups.length === 0) {
      return;
    }
    const currIndex = this.findSlideGroupIndex(this.currentSlideGroupId);
    if (currIndex === null && this.presentation.slideGroups.length > 0) {
      this.setCurrentSlideGroup(this.presentation.slideGroups[0]);
      return;
    }
    if (this.presentation.slideGroups.length > currIndex + 1) {
      this.setCurrentSlideGroup(this.presentation.slideGroups[currIndex + 1]);
    }
  }

  prevSong() {
    if (this.presentation.slideGroups.length === 0) {
      return;
    }
    const currIndex = this.findSlideGroupIndex(this.currentSlideGroupId);
    if (currIndex === null && this.presentation.slideGroups.length > 0) {
      this.setCurrentSlideGroup(this.presentation.slideGroups[0]);
      return;
    }
    if (currIndex - 1 >= 0) {
      this.setCurrentSlideGroup(this.presentation.slideGroups[currIndex - 1]);
    }
  }

  findSlideGroupIndex(songId) {
    let i = 0;
    for (const sg of this.presentation.slideGroups) {
      if (sg.getId() === songId) {
        return i;
      }
      i++;
    }
    return null;
  }

  public setCurrentSlideGroup(slideGroup: SlideGroup) {
    this.setSlideGroup.emit(slideGroup);
    this.currentSlideGroupId = slideGroup.getId();
  }

  async editSlideGroup(sg: SlideGroup) {
    console.log(sg);
    if (sg instanceof SongSlideGroup) {
      const song = await this.songProvier.getSong(sg.songId, sg.keyTone);
      this.songEditController.editSongDialog(song, song => {
        this.snackBar.open("Utwór \"" + song.title + "\" został zapisany", 'ok', {
          duration: 3000
        });
      });
    }
  }

  isImage(sg: SlideGroup): boolean {
    return sg instanceof ImageSlideGroup;
  }

  getImageUrl(sg: SlideGroup) {
    if (sg instanceof ImageSlideGroup) {
      return this.resourceFileSystemService.getPathForResource(sg.image)
    }
    return '';
  }
}
