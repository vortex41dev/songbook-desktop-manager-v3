import {AfterViewInit, Component, HostListener, Inject, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {ElectronService} from './providers/electron.service';
import {MatDialog, MatSidenav, MatSnackBar} from "@angular/material";
import {
  PRESENTER_STATE_PRES_NAV,
  PresenterSidebarComponent
} from "./components/presenter-sidebar/presenter-sidebar.component";
import {Song} from "../Model/Song";
import {SlideshowControllerService} from "./services/slideshow-controller.service";
import {SongEditControllerTraitService} from "./services/song-edit-controller-trait.service";
import {LocalStorageService} from "angular-2-local-storage";
import {
  DISPLAY_MODE_SLIDESHOW, DISPLAY_MODE_SONGBOOK, MSG_CHANGE_SLIDE_CODE_NEXT,
  MSG_CHANGE_SLIDE_CODE_PREV, MSG_CHANGE_SLIDE_CODE_RESTART
} from "../settings";
import {DOCUMENT} from "@angular/common";
import {LocalSongsProviderService} from "./services/data-providers/local/local-songs-provider.service";
import {SongTransponerService} from "./services/data-providers/local/song-transponer.service";
import {ImportExportComponent} from "./components/dialog/import-export/import-export.component";
import {BluetoothControllerService} from "./services/comunication/bluetooth-controller.service";
import {InfoDialogComponent} from "./components/dialog/info-dialog/info-dialog.component";
import {
  createSongSlideGroup,
  SLIDE_GROUP_TYPE_SONG,
  SlideGroup,
  SongSlideGroup,
} from "../Model/SlideGroup";
import {SlideShowSlideGroupLivePreviewComponent} from "./components/slide-show-slide-group-live-preview/slide-show-slide-group-live-preview.component";
import {UUID} from "angular2-uuid";

export const DUR_TONATION = ['A', 'B', 'H', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'];
export const MOL_TONATION = ['a', 'b', 'h', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#'];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {


  @ViewChild('sidenav', {static: true})
  private sideNav: MatSidenav;

  @ViewChild('slideShow', {static: true})
  public slideShowComponent: SlideShowSlideGroupLivePreviewComponent;

  @ViewChild('presentationPresenterSidebar', {static: true})
  private presentationPresenterSidebar: PresenterSidebarComponent;

  @Input()
  songs: Song[] = [];

  private _slidesVisibleGuard: boolean = true;
  private _slidesVisible: boolean = false; // Affects view only if displayMode is slideshow
  @Input()
  set slidesVisible(visibility: boolean) {
    this._slidesVisible = visibility;
    if (this._slidesVisibleGuard) {
      this.slideshowController.sendSlideVisibility(visibility);
    }
  }

  get slidesVisible(): boolean {
    return this._slidesVisible;
  }

  @Input()
  splitMode: boolean = true;

  toggleSplitMode() {
    this.splitMode = !this.splitMode;
    this.setSavedValue('splitMode', this.splitMode);
  }

  toggleSlidesVisibility() {
    this.slidesVisible = !this.slidesVisible;
  }

  toggleNightMode() {
    this.nightModeEnabled = !this.nightModeEnabled;
    this.setSavedValue('nightMode', this.nightModeEnabled);
  }


  private _chordsVisible = false;
  @Input()
  get chordsVisible(): boolean {
    return this._chordsVisible;
  };

  set chordsVisible(value: boolean) {
    this._chordsVisible = value;
    this.saveChordsVisibility(value);
  };

  @Input()
  nightModeEnabled: boolean = false;

  @Input()
  presentationNavMode: boolean = true;


  private __dontUpdateKeyToneGuard = false;
  private __dontUpdateSongByKeyTone = false;
  private __dontSendSongChangedGuard = false;

  private _currentSong: Song;

  @Input()
  get currentSong(): Song {
    return this._currentSong;
  }

  set currentSong(song: Song) {
    this._currentSong = song;
    if (!this.__dontUpdateKeyToneGuard) {
      this.__dontUpdateSongByKeyTone = true;

      const tone = SongTransponerService.getSongKeyTone(song);
      if (tone) {
        if (tone.toUpperCase() === tone) {
          this.toneScale = DUR_TONATION;
        } else {
          this.toneScale = MOL_TONATION;
        }
      }
      this.keyTone = tone;
      this.__dontUpdateSongByKeyTone = false;
    }
    if (song && !this.__dontSendSongChangedGuard) {
      const tone = AppComponent.serializeToneForUrl(SongTransponerService.getSongKeyTone(song));
      this.slideshowController.sendChangeSong(tone, song);
    }
  }

  private _currentSlideGroup: SlideGroup;
  @Input()
  get currentSlideGroup(): SlideGroup {
    return this._currentSlideGroup;
  }

  set currentSlideGroup(slideGroup: SlideGroup) {
    this._currentSlideGroup = slideGroup;
    this.slideshowController.sendChangeSlideGroup(slideGroup);
    if (slideGroup.type !== SLIDE_GROUP_TYPE_SONG) {
      return;
    }
    if (!this.__dontUpdateKeyToneGuard) {
      this.__dontUpdateSongByKeyTone = true;
      const songSlideGroup = slideGroup as SongSlideGroup;
      this.songProvier.getSong(songSlideGroup.songId, songSlideGroup.keyTone)
        .then(song => {
          if (!song) {
            return;
          }
          console.log(song);
          const tone = SongTransponerService.getSongKeyTone(song);
          if (tone) {
            if (tone.toUpperCase() === tone) {
              this.toneScale = DUR_TONATION;
            } else {
              this.toneScale = MOL_TONATION;
            }
          }
          this.keyTone = tone;
          if (song && !this.__dontSendSongChangedGuard) {
            const tone = AppComponent.serializeToneForUrl(SongTransponerService.getSongKeyTone(song));
            console.log(song);
            this.slideshowController.sendChangeSong(tone, song);
          }
        })
        .catch(e => {
        })
        .then(e => {
          this.__dontUpdateSongByKeyTone = false;
        });
    }
  }

  @Input()
  toneScale = DUR_TONATION;

  @ViewChild('songsList', {static: false})
  private songsListComponent;

  _keyTone: string = null;
  @Input()
  get keyTone(): string {
    return this._keyTone;
  }

  set keyTone(tone: string) {
    const newKeyTone = tone && AppComponent.serializeToneForUrl(tone) || null;
    if (this._keyTone === newKeyTone) {
      return;
    }
    this._keyTone = newKeyTone;
    if (!this.__dontUpdateSongByKeyTone) {
      this.transposeSong(this.currentSong, this._keyTone);
    }
  }

  @Input()
  _displayMode = DISPLAY_MODE_SONGBOOK;
  @Input()
  get displayMode() {
    return this._displayMode;
  }

  set displayMode(mode) {
    this._displayMode = mode;
    this.saveDisplayMode(mode);
  }

  @Input()
  keyboardNavigationEnabled: boolean = true;

  @Input()
  syncEnabled: boolean = true;

  constructor(public electronService: ElectronService,
              private songProvier: LocalSongsProviderService,
              private storage: LocalStorageService,
              private songEditController: SongEditControllerTraitService,
              private snackBar: MatSnackBar,
              private slideshowController: SlideshowControllerService,
              @Inject(DOCUMENT) private document,
              private bluetoothController: BluetoothControllerService,
              private _ngzone: NgZone,
              private dialog: MatDialog) {

    if (electronService.isElectron()) {
      console.log('Mode electron');
      // Check if electron is correctly injected (see externals in webpack.config.js)
      console.log('c', electronService.ipcRenderer);
      // Check if nodeJs childProcess is correctly injected (see externals in webpack.config.js)
      console.log('c', electronService.childProcess);
    } else {
      console.log('Mode web');
    }
  }

  ngOnInit(): void {
    this.songProvier.getSongsList().then(songs => {
      this.songs = songs;
    });
    this.chordsVisible = this.checkIfChordsWereVisible();
    this.displayMode = this.getSavedDisplayMode();

    this.nightModeEnabled = <boolean>this.getSavedValue('nightMode');

    this.presentationNavMode = <boolean>this.getSavedValue('presentationMode');

    this.splitMode = <boolean>this.getSavedValue('splitMode');

    this.presentationPresenterSidebar.parentController = this;

    this.setupSlideshowController();
  }

  ngAfterViewInit() {
    this.setupKeyboardNavigationEnable();
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('loader');
    setTimeout(() => {
      document.getElementById('csloader').remove();
    }, 500);
  }

  private setupSlideshowController() {
    this.slideshowController.connect('test');

    this.slideshowController.slideChange.subscribe(nextSlide => {
      switch (nextSlide.dir) {
        case MSG_CHANGE_SLIDE_CODE_NEXT:
          this.slideShowComponent.sectionChangeNext();
          break;
        case MSG_CHANGE_SLIDE_CODE_PREV:
          this.slideShowComponent.sectionChangePrev();
          break;
        case MSG_CHANGE_SLIDE_CODE_RESTART:
          this.slideShowComponent.restartSlideShow();
          break;
      }
    });
    this.slideshowController.songChange.subscribe(songIdTone => {
      this.songProvier
        .getSong(songIdTone.id, songIdTone.tone)
        .then(song => {
          this.__dontSendSongChangedGuard = true;
          this.currentSong = song;
          this.__dontSendSongChangedGuard = false;
        });
    });

    this.slideshowController.slideVisibility.subscribe(visibility => {
      this._slidesVisibleGuard = false;
      this.slidesVisible = visibility;
      this._slidesVisibleGuard = true;
    });


    this.slideshowController.websockService.connectedToServer.subscribe(e => {
      const songCopy: any = this.currentSong;
      if (!songCopy) {
        return;
      }
      songCopy.currentSectionNr = this.slideShowComponent.getCurrentSectionIndex();
      const tone = AppComponent.serializeToneForUrl(SongTransponerService.getSongKeyTone(songCopy));
      this.slideshowController.sendChangeSong(tone, songCopy);
      this.slideshowController.sendSlideVisibility(this.slidesVisible);
    });

    this.bluetoothController.prevSlidePressed.subscribe(x => this.presenterPrevSlide());
    this.bluetoothController.prevSongPressed.subscribe(x => this.presenterPrevSong());
    this.bluetoothController.nextSlidePressed.subscribe(x => this.presenterNextSlide());
    this.bluetoothController.nextSongPressed.subscribe(x => this.presenterNextSong());
    this.bluetoothController.restartPressed.subscribe(x => this.presenterRestartSlideshow());
    this.bluetoothController.toggleVisibilityPressed.subscribe(x => this.presenterToggleSlideVisibility());
  }

  private setupKeyboardNavigationEnable() {
    this.sideNav.openedStart.subscribe(x => {
      this.keyboardNavigationEnabled = false;
    });

    this.sideNav.closedStart.subscribe(x => {
      this.keyboardNavigationEnabled = true;
    });

    this.songEditController.dialogClosed.subscribe(x => {
      this.keyboardNavigationEnabled = true;
    });

    this.songEditController.dialogOpened.subscribe(x => {
      this.keyboardNavigationEnabled = false;
    });
  }

  createNewSong() {
    this.songEditController.createNewDialog(song => {
      this.songs.push(song);
      this.sortSongs();
      this.songsListComponent.currentSong = song;
      this.snackBar.open("Utwór \"" + song.title + "\" został utworzony", 'ok', {
        duration: 3000
      });
    });
  }

  editCurrentSong() {
    this.songEditController.editSongDialog(this.currentSong, song => {
      this.sortSongs();
      this.snackBar.open("Utwór \"" + song.title + "\" został zapisany", 'ok', {
        duration: 3000
      });
    });
  }


  deleteSong() {
    if (this.currentSong) {
      this.songEditController.deleteSongDialog(this.currentSong, (song) => {
        this.songProvier.deleteSong(song).then(x => {
          const index = this.songs.indexOf(song, 0);
          if (index > -1) {
            this.songs.splice(index, 1);
          }
          this.snackBar.open("Utwór \"" + song.title + "\" został usunięty", 'ok', {
            duration: 3000
          });
          this.currentSong = null;
        });
      });
    }
  }

  private checkIfChordsWereVisible(): boolean {
    return !!this.storage.get('chordsVisible');
  }

  private saveChordsVisibility(value: boolean) {
    this.storage.set('chordsVisible', value);
  }

  private getSavedDisplayMode(): string {
    return <string>this.storage.get('displayMode') || DISPLAY_MODE_SONGBOOK;
  }

  private saveDisplayMode(mode: string) {
    this.storage.set('displayMode', mode);
  }


  @HostListener('window:keydown', ['$event'])
  windowKeydownEventHandler($event) {
    console.log($event);
    if (this.sideNav.opened && !this.keyboardNavigationEnabled &&
      ($event.code === 'Escape' || $event.code === 'Backquote')) {
      this.sideNav.toggle();
      return;
    }
    if (this.keyboardNavigationEnabled) {
      switch ($event.code) {
        case 'Backquote':
          this.sideNav.toggle();
          break;
        case 'ArrowRight':
        case 'Space':
          this.presenterNextSlide();
          break;
        case 'PageDown':
          this.presenterNextSlide(true);
          break;
        case 'ArrowLeft':
          this.presenterPrevSlide();
          break;
        case 'PageUp':
          this.presenterPrevSlide(true);
          break;
        case 'KeyP':
        case 'F5':
        case 'Escape':
          this.toggleDisplayMode();
          break;
        case 'KeyB':
          this.presenterToggleSlideVisibility();
          break;
        case 'KeyA':
          this.chordsVisible = !this.chordsVisible;
          break;
        case 'KeyR':
          this.presenterRestartSlideshow();
          break;
        case 'Comma':
          this.presenterPrevSong();
          break;
        case 'Period':
          this.presenterNextSong();
          break;
        case 'F1':
          this.openAboutDialog();
          break;
      }
    }
  }

  private toggleDisplayMode() {
    this.displayMode = this.displayMode === DISPLAY_MODE_SONGBOOK ? DISPLAY_MODE_SLIDESHOW : DISPLAY_MODE_SONGBOOK;
  }

  private forceRefreshView(callback: () => void) {
    this._ngzone.run(callback)
  }

  private presenterNextSlide(skipToNextSlideGroup: boolean = false) {
    this.forceRefreshView(() => {
      const result = this.slideShowComponent.sectionChangeNext();
      if (skipToNextSlideGroup && !result) {
        this.presenterNextSong();
        return;
      }
      this.slideshowController.sendNextSlide();
    });
  }

  private presenterPrevSlide(skipToPrevSlideGroup: boolean = false) {
    this.forceRefreshView(() => {
      const result = this.slideShowComponent.sectionChangePrev();
      if (skipToPrevSlideGroup && !result) {
        this.presenterPrevSong();
        return;
      }
      this.slideshowController.sendPrevSlide();
    });
  }

  private presenterToggleSlideVisibility() {
    this.forceRefreshView(() => {
      this.slidesVisible = !this.slidesVisible;
    });
  }

  private presenterRestartSlideshow() {
    this.forceRefreshView(() => {
      this.slideShowComponent.restartSlideShow();
      this.slideshowController.sendRestartSlideShow();
    });
  }

  private presenterPrevSong() {
    if (typeof (this.presentationPresenterSidebar) !== 'undefined'
      && typeof (this.presentationPresenterSidebar.presentationPreview) !== 'undefined'
    ) {
      this.forceRefreshView(() => {
        this.presentationPresenterSidebar.presentationPreview.prevSong();
      });
    }
  }

  private presenterNextSong() {
    if (typeof (this.presentationPresenterSidebar) !== 'undefined'
      && typeof (this.presentationPresenterSidebar.presentationPreview) !== 'undefined'
    ) {
      this.forceRefreshView(() => {
        this.presentationPresenterSidebar.presentationPreview.nextSong();
      });
    }
  }

  private transposeSong(currentSong: Song, keyTone: string) {
    if (!currentSong) {
      return;
    }
    this.songProvier.getSong(currentSong.id, keyTone).then(song => {
      const songPos = this.songs.findIndex(s => s.id === currentSong.id);
      this.songs[songPos] = song;
      this.__dontUpdateKeyToneGuard = true;
      this.currentSong = song;
      this.__dontUpdateKeyToneGuard = false;
    });
  }

  private sortSongs() {
    this.songs.sort((l: Song, r: Song) => r.title > l.title ? -1 : r.title < l.title ? 1 : 0);
  }

  private static serializeToneForUrl(tone: string) {
    return tone ? tone.replace('#', 'i') : null;
  }

  private getSavedValue(key: string) {
    return this.storage.get(key);
  }

  private setSavedValue(key: string, value: any) {
    this.storage.set(key, value);
  }

  public togglePresentationMode() {
    this.presentationNavMode = !this.presentationNavMode;
    this.setSavedValue('presentationMode', this.presentationNavMode);
  }


  public openImportExportDialog() {
    const dlg = this.dialog.open(ImportExportComponent);
    dlg.afterClosed().subscribe(x => {

    });
  }

  public openAboutDialog() {
    const dlg = this.dialog.open(InfoDialogComponent);
  }

  public setCurrentSongFromSidebar(song: Song) {
    let songSlideGroup = createSongSlideGroup(song);
    songSlideGroup.id = UUID.UUID();
    this.currentSlideGroup = songSlideGroup;
  }

  public toggleSync() {
    this.syncEnabled = !this.syncEnabled;
  }
}
