export class FileListConverter {
    static toArray(fileList: FileList): File[] {
        const arr = [];
        for (let i = 0; i < fileList.length; i++) {
            arr.push(fileList[i]);
        }
        return arr;
    }
}
