export class TextFileReader {
    static readTextFile(file): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const reader = new FileReader();
            // If we use onloadend, we need to check the readyState.
            reader.onloadend = (evt) => {
                const target: any = <any>evt.target;
                if (target.readyState === 2) { // done
                    resolve(target.result);
                } else {
                    reject();
                }
            };
            reader.readAsText(file);
        });
    }
}

export class BinaryFileReader {
  static readFileAsArrayBuffer(file): Promise<ArrayBuffer> {
    return new Promise<ArrayBuffer>((resolve, reject) => {
      const reader = new FileReader();
      // If we use onloadend, we need to check the readyState.
      reader.onloadend = (evt) => {
        const target: any = <any>evt.target;
        if (target.readyState === 2) { // done
          resolve(target.result);
        } else {
          reject();
        }
      };
      reader.readAsArrayBuffer(file);
    });
  }

  static readFileAsBinaryString(file): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      // If we use onloadend, we need to check the readyState.
      reader.onloadend = (evt) => {
        const target: any = <any>evt.target;
        if (target.readyState === 2) { // done
          resolve(target.result);
        } else {
          reject();
        }
      };
      reader.readAsBinaryString(file);
    });
  }
}
