import {Song} from "./Song";
import {SongTransponerService} from "../app/services/data-providers/local/song-transponer.service";
import {UUID} from "angular2-uuid";

export const SLIDE_GROUP_TYPE_SONG = 'song';
export const SLIDE_GROUP_TYPE_IMAGE = 'image';

export interface SlideGroupBaseRepresentation {
  id: string;
  name: string;
  type: string;
}

export abstract class SlideGroup implements SlideGroupBaseRepresentation {
  id: string;
  name: string;
  type: string;

  abstract getId();

  abstract getName(): string;
}

export interface SongSlideGroupBaseRepresentation extends SlideGroupBaseRepresentation {
  songId: string;
  keyTone: string;
  order: number;
}

export class SongSlideGroup extends SlideGroup implements SongSlideGroupBaseRepresentation {
  songId: string;
  keyTone: string;
  order: number;

  constructor() {
    super();
    this.type = SLIDE_GROUP_TYPE_SONG;
  }

  getId() {
    return this.songId;
  }

  public static buildFromSlideGroupRepresentation(songSlideRepresentation: SongSlideGroupBaseRepresentation): SongSlideGroup {
    const s = new SongSlideGroup();

    s.id = songSlideRepresentation.id || songSlideRepresentation.songId;
    s.name = songSlideRepresentation.name;

    s.songId = songSlideRepresentation.songId;
    s.keyTone = songSlideRepresentation.keyTone;
    s.order = songSlideRepresentation.order;

    return s;
  }

  getName(): string {
    return (this.order ? this.order + '. ' : '') + this.name;
  }
}

export interface ImageSlideGroupBaseRepresentation extends SlideGroupBaseRepresentation {
  image: string;
}

export class ImageSlideGroup extends SlideGroup implements ImageSlideGroupBaseRepresentation {
  image: string;

  constructor(id, name, image) {
    super();
    this.id = id;
    this.type = SLIDE_GROUP_TYPE_IMAGE;
    this.name = name;
    this.image = image;
  }

  getId() {
    return this.id;
  }

  public static buildFromSlideGroupRepresentation(imageSlideRepresentation: ImageSlideGroupBaseRepresentation): ImageSlideGroup {
    return new ImageSlideGroup(imageSlideRepresentation.id, imageSlideRepresentation.name, imageSlideRepresentation.image);
  }

  getName(): string {
    return this.name;
  }

  clone(): ImageSlideGroup {
    return new ImageSlideGroup(UUID.UUID(), this.name, this.image);
  }
}

export function createSongSlideGroup(song: Song) {
  const s = new SongSlideGroup();
  s.id = song.id;
  s.songId = song.id;
  const keyTone = SongTransponerService.getSongKeyTone(song);
  s.keyTone = keyTone ? keyTone.replace('#', 'i') : null;
  s.name = song.title;
  s.type = SLIDE_GROUP_TYPE_SONG;
  s.order = song.order;
  return s;
}

