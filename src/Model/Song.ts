export interface SongSection {
    id: string,
    text: string,
    chords: string,
    fontSize: string
}

export interface Song {
    id: string;
    categories: string[];
    title: string;
    order: number;
    sections: SongSection[];
}

export function createSong(): Song {
    const song = {
        id: '0',
        order: 0,
        categories: [],
        title: '',
        sections: [createSongSection()]
    };
    return <Song>song;
}

export interface SongRepresentation {
    songId: string;
    keyTone: string;
    order: number;
}

export function createSongRepresentation(song: Song, keyTone: string, order: number): SongRepresentation {
    return <SongRepresentation>{
        songId: song.id,
        keyTone: keyTone,
        order: order
    };
}

/**
 * Creates empty song section when elem == null, otherwise creates copy of song section
 * @param elem
 * @return SongSection
 */
export function createSongSection(elem: SongSection = null): SongSection {
    if (elem) {
        return <SongSection>{
            id: '0',
            text: elem.text,
            chords: elem.chords,
            fontSize: elem.fontSize,
        };
    } else {
        return <SongSection>{
            id: '0',
            text: '',
            chords: '',
            fontSize: null,
        };
    }
}
