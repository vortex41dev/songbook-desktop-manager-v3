import {SlideGroup, SongSlideGroup} from "./SlideGroup";

export class Presentation {
    id: any;
    name: string;
    slideGroups: SlideGroup[];
}

export function createPresentation(): Presentation {
    const p = new Presentation();
    p.id = 0;
    p.name = '';
    p.slideGroups = [];
    return p;
}

export interface PresentationRepresentation {
    id: string;
    name: string;
    slideGroups: any[];
}
