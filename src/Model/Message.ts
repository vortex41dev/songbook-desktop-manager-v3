export class Message {
    messageType: number;
    data: any;

    constructor(type, data) {
        this.messageType = type;
        this.data = data;
    }
}
