"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SERVER_PORT = 10100;
// export const SERVER_IP = '127.0.0.1';
// let matches = location.href.match(/https?:\/\/([^:/]+)(:[0-9]+)?/);
exports.SERVER_IP = "127.0.0.1"; // matches[1];
exports.SERVER_ADDRESS = 'ws://' + exports.SERVER_IP + ':' + exports.SERVER_PORT;
exports.DISPLAY_MODE_SONGBOOK = 'songbook';
exports.DISPLAY_MODE_SLIDESHOW = 'slideshow';
exports.MSG_CHANGE_SLIDE = 1;
exports.MSG_CHANGE_SONG = 2;
exports.MSG_CHANGE_SLIDES_VISIBILITY = 3;
exports.MSG_CHANGE_SLIDE_CODE_PREV = 2;
exports.MSG_CHANGE_SLIDE_CODE_NEXT = 3;
exports.MSG_CHANGE_SLIDE_CODE_RESTART = 4;
exports.BLUETOOTH_CONTROLLER_SETTINGS = {
    btDestAddress: "98:D3:31:FC:4B:C9",
    btDestChannel: 1,
    service: "00001101-0000-1000-8000-00805f9b34fb"
};
exports.DEFAULT_SONG_SECTION_FONT_SIZE = '4.5vw';
//# sourceMappingURL=settings.js.map